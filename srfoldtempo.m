function r=srfoldtempo(a,varargin)
%A=SRFOLD(B)			reshapes data for SR symmetry
%
%	A(64*n,1) -> R{n,64}
%

sym=~getflag(varargin,'nosym');
nb=numel(a);

if isvector(a)
    if nb == 802
        r=reshape(a(2:end-1),[],32);
        r([1:2 4 6 8 18 20 22],1)=NaN;
        r([end-21 end-19 end-17 end-7 end-5 end-3 end-1:end])=NaN;
        if sym
            r=setinj(r,[0:2 4 6 8 18 20 22],a);
        end
    elseif nb == 514        % Special case of quadrupoles (514)
        r=reshape(a(2:end-1),[],32);
        r(1:2,1)=NaN;
        r(end-1:end,end)=NaN;
        if sym
            r=setinj(r,0:2,a);
        end
    elseif nb == 384        % Special case for H steerers (384)
        r=reshape(a,[],32);
        r([5 7:8],:)=[];    % Ignore DQs
        r(1:3,1)=NaN;
        r(end-2:end,end)=NaN;
        if sym
            r=setinj(r,0:2,a);
        end
    elseif nb == 288        % Special case for V steerers (288)
        r=reshape(a,[],32);
        r(1:3,1)=NaN;
        r(end-2:end,end)=NaN;
        if sym
            r=setinj(r,0:2,a);
        end
    elseif nb == 192        % Special case for sextupoles (192)
        r=reshape(a,[],32);
        r(1:6,1)=NaN;
        r(end-5:end,end)=NaN;
        if sym
            r=setinj(r,0:5,a);
        end
    else                    % General case
        n2=numel(a)/32;
        r=reshape(a(:),n2,32);
        if sym
            r=symfold(r);
        end
    end
else
    error('Not implemented for matrices')
end

    function s=symfold(b)
        l2=size(b,1);
        if bitand(l2,1) % odd number
            l=(l2+1)/2;
            v=[b(1:l,:);b(l2:-1:l+1,:);NaN(1,32)];
        else
            l=l2/2;
            v=[b(1:l,:);b(l2:-1:l+1,:)];
        end
        s=reshape(v,l,64);
    end

    function k=setinj(h,id,aa)
        ninj=length(id);
        inj=NaN(ninj,64);
        inj(1:ninj,1)=aa(1+id);
        inj(1:ninj,end)=aa(length(aa)-id);
        k=[symfold(h);inj];
    end

% if isvector(a)
%     n=numel(a)/32;
%     b=reshape(a(:),2*n,16);
%     r=reshape([b(1:n,:);b(2*n:-1:n+1,:)],n,32);
% elseif nargin<2 || dim==1
%     sz=size(a);
%     n=sz(1)/32;
%     a2=reshape(a,2*n,[]);
%     r=reshape(a2([1:n 2*n:-1:n+1],:),[n 32 sz(2:end)]);
% else
%     nd=ndims(a);
%     ord1=[dim 1:dim-1 dim+1:nd];
%     ord2=[dim dim+1 1:dim-1 1+(dim+1:nd)];
%     r=ipermute(sr.fold(permute(a,ord1)),ord2);
% end
end
