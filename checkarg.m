function [ax,args] = checkarg(args,varargin)
%GETARGS Check for 'axes' argument
%
%AXES=GETCHECKEDARG(VARARGIN)
%[AXES,REMAIN]=GETCHECKEDARG(VARARGIN)
%   Check for Axes as the 1st argument
%
%See also GETFLAG, GETOPTION, GETARGS

[check,varargs]=getoption(varargin,'check',@checkaxes);
[generate,varargs]=getoption(varargs,'generate',@currentaxes); %#ok<ASGLU>
if ~isempty(args) && check(args{1})
    ax=args{1};
    args=args(2:end);
else
    ax=generate();
end
    function ok=checkaxes(arg)
        ok=all(isgraphics(arg,'axes'));
    end
    function ax=currentaxes
        ax=gca;
    end
        
end
