function show_progress(step,message)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
persistent cpt cptmax

if nargin >= 2
    cpt=0;
    cptmax=step;
    fprintf('%s: %4i/%4i',message,cpt,cptmax);
else
    if nargin < 1
        step=1;
    end
    cpt=cpt+step;
    %fprintf('%4i/%4i\n',cpt,cptmax);
    fprintf('\b\b\b\b\b\b\b\b\b%4i/%4i',cpt,cptmax);
end
if cpt==cptmax
    fprintf('\n');
end
end
