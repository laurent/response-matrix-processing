function mask=extend_mask(desired,submask)
%EXTEND_MASK
desired=logical_mask(desired,[1 sum(submask)]);
mask=submask;
mask(submask)=desired;
end
