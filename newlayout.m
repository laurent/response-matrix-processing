function layout=newlayout(m,n,varargin)
%NEWLAYOUT	Find or create layout

odd=logical(bitget(length(varargin),1));
[name,varargs]=checkarg(varargin,'','check',@(arg) odd,'generate',@() '');
if isempty(name)
    % Create a new figure
    figure();
else
    fig=findobj('Type','figure','Name',name);
    if isempty(fig)
        % Create a new figure with specified name
        figure('Name',name);
    else
        % Reuse the figure with specified name
        figure(fig(1));
    end
end
layout=tiledlayout(m,n,varargs{:});
axtoolbar(layout,{'export','datacursor','pan','zoomin','zoomout','restoreview'});
end
