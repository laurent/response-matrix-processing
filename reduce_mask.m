function mask=reduce_mask(atmodel,desired,submask,available)
%REDUCE_MASK
if nargin < 4
    available=submask;
end
desired=logical_mask(desired,size(submask));
missing=desired & ~available;
if any(missing)
    missnames=atmodel.getfieldvalue(missing,'FamName');
    warning('ParameterFit:NotAvailable',...
        '%s not available and ignored',sprintf(' %s',missnames{:}));
    desired=desired & available;
end
mask=desired(submask);
end
