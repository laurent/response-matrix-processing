classdef SRCorrection
    %Correction object
    
    properties (SetAccess=immutable)
        mask            % Location of correctors
        perturb_fun     % Function applying the correction to each element
    end
    
    properties
        strength        % Correction strength
    end
    
    methods
        function obj = SRCorrection(mask,perturb_fun,varargin)
            %SRCorrection     Correction
            %
            %SRCorrection(MASK,PERTURB_FUN,STRENGTH)
            %MASK:          Location of corrections
            %PERTURB_FUN:   Function applying the correction to each element
            %STRENGTH:      Correction strength (Default: 0)
            
            [strength,varargs]=getargs(varargin,0); %#ok<ASGLU>
            obj.mask=mask;
            obj.perturb_fun=perturb_fun;
            obj.strength(1:sum(obj.mask),1)=strength;
        end
        
        function apply(obj,atmodel)
            %APPLY  Apply a correction to AT model
            for ob=obj
                atmodel.substitute(ob.mask,ob.perturb_fun,num2cell(ob.strength));
            end
        end
        
        function newobj=plus(obj,strength)
            %PLUS   Implement correction1 + correction2
            if isa(strength,'SRCorrection')
                strength=strength.strength;
            end
            newobj=SRCorrection(obj.mask,obj.perturb_fun,obj.strength+strength);
        end
         
        function newobj=minus(obj,strength)
            %PLUS   Implement correction1 - correction2
            if isa(strength,'SRCorrection')
                strength=strength.strength;
            end
            newobj=SRCorrection(obj.mask,obj.perturb_fun,obj.strength-strength);
        end
        
        function newobj=uminus(obj)
            %PLUS   Implement correction1 -correction
            newobj=arrayfun(@(ob) SRCorrection(ob.mask,ob.perturb_fun,-ob.strength),obj);
        end
       
        function obj=set.strength(obj,value)
            obj.strength(1:sum(obj.mask),1)=value; %#ok<MCSUP> %Check size of the values
        end
    end
    
    methods (Static)
        function corr=QuadCorrection(atmodel,fname)
            %QUADCORRECTION     Create a quadrupole correction object
            quadmask=atmodel.select('qp');
            dqmask=atmodel.select('dq');
            cormask=quadmask | dqmask;
            corr=SRCorrection(cormask, @SRCorrection.quad);
            if nargin >= 2                  % Get values from measurement file
                select=quadmask(cormask);   % Get only quadrupoles
                Nq=textscan(fopen(fname,'r'),'%s %f','HeaderLines',7);
                corr.strength(select) = Nq{2}(select);
            end

        end
        
        function corr=SkewCorrection(atmodel,fname)
            %SKEWCORRECTION     Create a skew quadrupole correction object
            shmask=atmodel.select('steerh');
            sfmask=atmodel.select('sx');
            corr=SRCorrection(shmask | sfmask, @SRCorrection.skew);
            if nargin >= 2      % Get values from measurement file
                Sq=textscan(fopen(fname,'r'),'%s %f','HeaderLines',7);
                corr.strength = Sq{2};
            end
        end
        
        function elem=quad(elem,strength)
            %QUAD  apply quadrupole correction
            elem.PolynomB(2)=elem.PolynomB(2) + strength/elem.Length;
            %fprintf('quad: %s, %f\n',elem.FamName,strength);
        end
        
        function elem=skew(elem,strength)
            %SKEW  apply skew quadrupole correction
            elem.PolynomA(2)=elem.PolynomA(2) + strength/elem.Length;
            %fprintf('skew: %s, %f\n',elem.FamName,strength);
        end
    end
    
end

