classdef autocell < handle
    %AUTOCELL	"magic" cell array
    %
    %An AUTOCELL cell array automatically fills its cells upon request
    %   using a function provided to the AUTOCELL constructor
    %
    %ac=AUTOCELL(@func,sz) creates an empty cell array of size sz
    %ac(i,j) will call func(i,j) if necessary to compute the desired cells
    %and return their contents
    
    properties (SetAccess=immutable)
        computef    % Compute function
        filename    % Optional file name
    end
    
    properties (SetAccess=immutable, GetAccess=protected)
        lineindex   % Line indexing
        columnindex % Column indexing
    end
    
    properties (SetAccess=private)
        data        % Underlying data storage
    end
    
    methods
        
        function obj=autocell(computef,varargin)
            %AUTOCELL   Create a "magic" cell array
            %
            %AUTOCELL(FILLFUNC,M,N,...) creates an AUTOCELL of sise MxNx...
            %AUTOCELL(FILLFUNC,[M N...]) creates an AUTOCELL of sise MxNx...
            %
            %FILLFUNC   Fill function
            %   FILLFUNC(I,J) will be called to fill the cell (I,J)
            %
            %AUTOCELL(FILLFUNC,CELL) creates an AUTOCELL initialized with CELL
            %
            %AUTOCELL(...,'FileName',FILENAME) creates a file-saving AUTOCELL
            %   If FILENAME exists, the AUTOCELL is initialized with its contents
            %   The file is then updated for each modification of the AUTOCELL
            %
            %AUTOCELL(...,'LineIndex',LIDX)
            %AUTOCELL(...,'ColumnIndex',CIDX)
            %   FILLFUNC(LIDX(I),CIDX(J)) will be called to file the cell (I,J)
            %       
            
            [lineindex,varargs]=getoption(varargin,'LineIndex',1);
            [columnindex,varargs]=getoption(varargs,'ColumnIndex',1);
            [filename,varargs]=getoption(varargs,'FileName',[]);
            obj.computef=computef;
            obj.filename=filename;
            
            if isempty(varargs)
                % Dimensions are specified by index lengths
                obj.data=cell([length(lineindex) length(columnindex)]);
                obj.lineindex=lineindex;
                obj.columnindex=columnindex;
            else
                % Dimensions are explicitly given
                if iscell(varargs{1})
                    obj.data=varargs{1};
                else
                    obj.data=cell(varargs{:});
                end
                [nl,nc]=size(obj.data);
                obj.lineindex=checkindex(lineindex,nl);
                obj.columnindex=checkindex(columnindex,nc);
            end
            
            if ~isempty(obj.filename)
                try
                    f=load(filename);
                    if any(size(f.data)~=size(obj.data))
                        warning('autocell:WrongSize','%s has incompatible size: ignored',filename);
                    elseif any(obj.lineindex~=f.lineindex)
                        warning('autocell:WrongSize','%s has incompatible lineindex: ignored',filename);
                    elseif any(obj.columnindex~=f.columnindex)
                        warning('autocell:WrongSize','%s has incompatible columnindex: ignored',filename);
                    else
                        obj.data=f.data;
                    end
                catch
                    warning('autocell:NoData','"%s": No initial data',filename);
                end
            end
            
            function id=checkindex(id,sz)
                if length(id) ~= sz
                    if all(id==1)   % Default value
                        id=1:sz;
                    else
                        error('AutoCell:WrongIndex',...
                            'Index length %d incompatible with cell dimension %d',length(id,sz));
                    end
                end
            end
        end
        
        function varargout=subsref(obj,s)
            switch s(1).type
                case {'()','{}'}
                    obj.fill(substruct('()',s(1).subs));
                    varargout={subsref(obj.data,s)};
                otherwise
                    varargout(1:nargout)={builtin('subsref',obj,s)};
            end
        end
        
        function obj=subsasgn(obj,s,value)
            switch s(1).type
                case {'()','{}'}
                    obj.data=subsasgn(obj.data,s,value);
                    fsave(obj,obj.data,obj.lineindex,obj.columnindex);
                otherwise
                    obj=builtin('subsasgn',obj,s,value);
            end
        end
        
        function n=numArgumentsFromSubscript(obj,s,c)
            switch s(1).type
                case {'()','{}'}
                    n=numArgumentsFromSubscript(obj.data,s,c);
                otherwise
                    n=builtin('numArgumentsFromSubscript',obj,s,c);
            end
        end
        
        function varargout=size(obj,varargin)
            [varargout{1:nargout}]=size(obj.data,varargin{:});
        end
        
        function n=numel(obj)
            n=numel(obj.data);
        end
        
    end
    
    methods (Access=protected)
        
        function fill(obj,s)
            % Look for missing cells
            missing=false(size(obj.data));
            missing=subsasgn(missing,s,cellfun(@isempty,subsref(obj.data,s)));
            cols=any(missing,1);
            lns=any(missing,2);
            % Compute missing cells
            if any(cols) && any(lns)
                obj.data(lns,cols)=obj.computef(obj.lineindex(lns),obj.columnindex(cols));
                fsave(obj,obj.data,obj.lineindex,obj.columnindex);
            end
        end
        
        function fsave(obj,data,lineindex,columnindex)
            if ~isempty(obj.filename)
                try
                    save(obj.filename,'data','lineindex','columnindex')
                catch
                    warning('autocell:CannotWrite','Impossible to save %s',obj.filename);
                end
            end
        end
        
    end
end

