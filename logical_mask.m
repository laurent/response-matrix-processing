function mask=logical_mask(input,sz)
if ~islogical(input)
    mask=false(sz);
    mask(input)=true;
else
    mask=input;
end
end
