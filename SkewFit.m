classdef SkewFit < ORMNewtonFit & CouplingProps

    methods
        function obj = SkewFit(measurement,varargin)
            %SKEWFIT	Fit of skew quadrupole strengths
            %
            %FIT=SKEWFIT(ORM)
            %
            % ORM:          Measured response matrix
            %
            %FIT=SKEWFIT(...[,keyword,value])
            %  keyword is in the following list:
            %
            % 'perturb_fun'	Perturbation function.
            %               @CouplingProps.setdks: use dKs/Kn as variable
            %               @CouplingProps.setks: use dKs as variable (Default)
            %
            % 'WorkDir'     Directory for storage of derivatives.
            %               Default: measurement directory
            %
            % 'select_errors'   Error selection
            %               Default: all
            %
            % All other arguments are used as (property_name, property_value)
            % pairs. This is useful when properties must be set in a well defined order.
            
            [perturb_fun,varargs]=getoption(varargin,'perturb_fun',@SkewFit.setks);
            quadmask=measurement.select('qp');
            sfmask=measurement.select('sx');
            dqmask=measurement.select('dq');
            errmask=quadmask | sfmask | dqmask;
            obj=obj@ORMNewtonFit(measurement,measurement,errmask,perturb_fun,1.0e-4,...
                'h_cell',@SkewFit.hderivs,'v_cell',@SkewFit.vderivs,...
                'f_cell',@SkewFit.fderivs,...
                varargs{:});
        end
        
    end
    
    methods (Static)
        
        % Functions for computing the derivatives. These functions should
        % be compiled / parallelized for efficiency
        
        
        function derivs=hderivs(refmodel,steerers,perturbs,~,~,~)
            %HDERIVS    Compute analytically horizontal orbit derivatives
            [~,derivs]=SkewFit.hvderivs(refmodel,steerers,perturbs);
        end
        
        function derivs=vderivs(refmodel,steerers,perturbs,~,~,~)
            %VDERIVS    Compute numerically vertical orbit derivatives
            [derivs,~]=SkewFit.hvderivs(refmodel,steerers,perturbs);
        end
        
        
        function derivs=fderivs(refmodel,~,perturbs,~,~,~)
            derivs=SkewFit.freqderivs(refmodel,perturbs);
        end
        
        function [vderivs, hderivs]=hvderivs(orm,shlist,skewlist)
            %HVDERIVS   Analytical Response Matrix Derivatives
            %
            % HVDERIVS returns the derivative of the steerers
            % response matrix when changing dipole angles and quadrupole gradients.
            %
            % from loco_fast_ESRFtest.m by Zeus Marti (ALBA CELLS, Barcellona)
            %
            % https://arxiv.org/pdf/1711.06589.pdf
            
            bpmmask=orm.select('bpm');
            
            Qx=orm.getmodelproperty('nuh');
            Qy=orm.getmodelproperty('nuv');
            
            sx=sin(pi*Qx);
            sy=sin(pi*Qy);
            
            bpmlist=find(bpmmask);
            
            nb=sum(bpmmask);
            nc=length(shlist);
            nfsq=length(skewlist);
            
            linbpm=orm.lindata(bpmmask);
            betab=cat(1,linbpm.beta);
            bxb=betab(:,1);
            byb=betab(:,2);
            mub=cat(1,linbpm.mu);
            fxb=mub(:,1);
            fyb=mub(:,2);
            
            linc=orm.lindata(shlist);
            betac=cat(1,linc.beta);
            bxc=betac(:,1)';
            byc=betac(:,2)';
            muc=cat(1,linc.mu);
            fxc=muc(:,1)';
            fyc=muc(:,2)';
            
            %% take out skew quad indices
            %[Ls,bxs,bys,fxs,fys]=optics(orm,skewlist);
            [Ls,bxs,bys,fxs,fys]=aveoptics(orm,skewlist);
            
            %% Sxy and Sxy skew term
            spos=sin(pi*(Qx+Qy));
            sneg=sin(pi*(Qx-Qy));
            
            Order_mj=heaviside(repmat(bpmlist,[1,nc,nfsq])-repmat(reshape(skewlist,[1,1,nfsq]),[nb,nc,1]));
            Order_mw=heaviside(repmat(shlist',[nb,1,nfsq])-repmat(reshape(skewlist,[1,1,nfsq]),[nb,nc,1]));
            Order_jw=heaviside(repmat(shlist',[nb,1,nfsq])-repmat(bpmlist,[1,nc,nfsq]));
            
            dfx_wj=repmat(fxb,[1,nc,nfsq])-repmat(fxc,[nb,1,nfsq])+2*pi*Qx*(Order_jw);
            dfx_mj=repmat(fxb,[1,nc,nfsq])-repmat(reshape(fxs,[1,1,nfsq]),[nb,nc,1])+2*pi*Qx*not(Order_mj);
            dfx_mw=repmat(fxc,[nb,1,nfsq])-repmat(reshape(fxs,[1,1,nfsq]),[nb,nc,1])+2*pi*Qx*not(Order_mw);
            
            tfx_wj=dfx_wj-pi*Qx;
            tfx_mj=dfx_mj-pi*Qx;
            tfx_mw=dfx_mw-pi*Qx;
            
            dfy_wj=repmat(fyb,[1,nc,nfsq])-repmat(fyc,[nb,1,nfsq])+2*pi*Qy*(Order_jw);
            dfy_mj=repmat(fyb,[1,nc,nfsq])-repmat(reshape(fys,[1,1,nfsq]),[nb,nc,1])+2*pi*Qy*not(Order_mj);
            dfy_mw=repmat(fyc,[nb,1,nfsq])-repmat(reshape(fys,[1,1,nfsq]),[nb,nc,1])+2*pi*Qy*not(Order_mw);
            
            tfy_wj=dfy_wj-pi*Qy;
            tfy_mj=dfy_mj-pi*Qy;
            tfy_mw=dfy_mw-pi*Qy;
            
            bys_re=repmat(reshape(bys.*Ls,[1,1,nfsq]),[nb,nc,1]);%.*Deltas(isskew)
            bxs_re=repmat(reshape(bxs.*Ls,[1,1,nfsq]),[nb,nc,1]);%.*Deltas(isskew)
            
            Byx=sqrt(repmat(byb*bxc,[1,1,nfsq]).*bxs_re.*bys_re);
            Aneg=(-cos(tfx_mj-tfy_mj-tfx_wj)/sx+cos(tfx_mw-tfy_mw-tfy_wj)/sy)/sneg;
            Apos=(cos(tfx_mj+tfy_mj-tfx_wj)/sx+cos(tfx_mw+tfy_mw+tfy_wj)/sy)/spos;
            hderivs=squeeze(num2cell(1i*Byx/8.*(Apos+Aneg),1));
            
            Bxy=sqrt(repmat(bxb*byc,[1,1,nfsq]).*bxs_re.*bys_re);
            Aneg=(cos(tfx_mj-tfy_mj+tfy_wj)/sy-cos(tfx_mw-tfy_mw+tfx_wj)/sx)/sneg;
            Apos=(cos(tfx_mj+tfy_mj-tfy_wj)/sy+cos(tfx_mw+tfy_mw+tfx_wj)/sx)/spos;
            vderivs=squeeze(num2cell(Bxy/8.*(Apos+Aneg),1));
            
            function [L,bx,by,fx,fy]=optics(orm,refpts)
                linfoc=orm.lindata(refpts);
                L=orm.getmodelattribute(refpts,'Length')';
                beta=cat(1,linfoc.beta);
                bx=beta(:,1)';
                by=beta(:,2)';
                mu=cat(1,linfoc.mu);
                fx=mu(:,1)';
                fy=mu(:,2)';
            end
            
            function [L,bx,by,fx,fy]=aveoptics(orm,refpts)
                linfoc=orm.avedata(refpts);
                tunes=2*pi*orm.getmodelproperty('tunes');
                L=orm.getmodelattribute(refpts,'Length')';
                bx=linfoc(:,2)';
                by=linfoc(:,3)';
                mu=linfoc(:,6:7).*tunes;
                fx=mu(:,1)';
                fy=mu(:,2)';
            end
            
            function y=heaviside(x)
                y=sign(x).*(1+signtilde(x))/2;
            end
            
            function y=signtilde(x)
                y=sign(x)-double(x==0);
            end
            
        end
        
        function derivs=freqderivs(refmodel,perturbs)
            %HVDERIVS   Analytical frequency response derivatives
            %
            % Analytic Response Matrix Derivative for dipoles and quadrupoles
            %
            % AnalyticResponseMatrixDerivative returns the derivative of the steerers
            % response matrix when changing dipole angles and quadrupole gradients.
            %
            % from loco_fast_ESRFtest.m by Zeus Marti (ALBA CELLS, Barcellona)
            %
            % https://arxiv.org/pdf/1711.06589.pdf
             
            alphainv=-1/refmodel.getmodelproperty('alpha');            
            bpmmask=refmodel.select('bpm');
            
            Qx=refmodel.getmodelproperty('nuh');
            Qy=refmodel.getmodelproperty('nuv');
            
            sx=sin(pi*Qx);
            sy=sin(pi*Qy);
            
            bpmlist=find(bpmmask);
            
            nb=sum(bpmmask);
            
            nfsq=length(perturbs);
            
            linbpm=refmodel.lindata(bpmmask);
            betab=cat(1,linbpm.beta);
            bxb=betab(:,1);
            byb=betab(:,2);
            mub=cat(1,linbpm.mu);
            fxb=mub(:,1);
            fyb=mub(:,2);
            
            %% take out skew quad indices
            [Ls,Kxs,bxs,bys,axs,ays,fxs,fys,dxs,dpxs,dys,dpys]=optics(refmodel,perturbs);
            %[Ls,Kxs,bxs,bys,axs,ays,fxs,fys,dxs,dpxs,dys,dpys]=aveoptics(refmodel,perturbs);
            sKxs=sqrt(Kxs);
            sKlxs=sKxs.*Ls;
            
            %% Sxy and Sxy skew term
            
            Order_mj=heaviside(repmat(bpmlist,[1,nfsq])-repmat(reshape(perturbs,[1,nfsq]),[nb,1]));
            
            dfx_mj=repmat(fxb,[1,nfsq])-repmat(reshape(fxs,[1,nfsq]),[nb,1])+2*pi*Qx*not(Order_mj);
            tfx_mj=dfx_mj-pi*Qx;
            dfy_mj=repmat(fyb,[1,nfsq])-repmat(reshape(fys,[1,nfsq]),[nb,1])+2*pi*Qy*not(Order_mj);
            tfy_mj=dfy_mj-pi*Qy;
            
            %% Dy and Dy skew quad term
            tfx_mjsq=squeeze(tfx_mj(:,:));
            tfy_mjsq=squeeze(tfy_mj(:,:));
            
            ssh=sin(sKlxs).*sinh(sKlxs);
            cch=cos(sKlxs).*cosh(sKlxs);
            sch=sin(sKlxs).*cosh(sKlxs);
            csh=cos(sKlxs).*sinh(sKlxs);
            
            TSx=dys./sqrt(bxs)./Kxs/2.*(ssh-cch+1)+dpys./sqrt(bxs)./sKlxs./Kxs/2.*(sch-csh);
            TCx=dys.*sqrt(bxs)./sKxs/2.*(sch+csh)-axs.*dpys./sqrt(bxs)./sKlxs./Kxs/2.*(sch-csh)-axs.*dys./sqrt(bxs)./Kxs/2.*(ssh-cch+1)+dpys.*sqrt(bxs)./Kxs/2.*(ssh+cch-1);
            TSy=dxs./sqrt(bys)./Kxs/2.*(ssh+cch-1)+dpxs./sqrt(bys)./sKlxs./Kxs/2.*(sch-csh);
            TCy=dxs.*sqrt(bys)./sKxs/2.*(sch+csh)-ays.*dpxs./sqrt(bys)./sKlxs./Kxs/2.*(sch-csh)-ays.*dxs./sqrt(bys)./Kxs/2.*(ssh+cch-1)+dpxs.*sqrt(bys)./Kxs/2.*(ssh-cch+1);
            
            dDx_ds=(sqrt(bxb)*TCx.*cos(tfx_mjsq)+sqrt(bxb)*TSx.*sin(tfy_mjsq))/2/sx;
            dDy_ds=(sqrt(byb)*TCy.*cos(tfy_mjsq)+sqrt(byb)*TSy.*sin(tfx_mjsq))/2/sy;
            derivs=num2cell(alphainv*(dDx_ds+1i*dDy_ds),1);
            
            function [L,Kx,bx,by,ax,ay,fx,fy,dx,dpx,dy,dpy]=optics(orm,refpts)
                linfoc=orm.lindata(refpts);
                L=orm.getmodelattribute(refpts,'Length')';
                K0=eps+orm.getmodelattribute(refpts,'PolynomB',{2},'Default',0)';
                %Kx=K0 + (orm.getmodelattribute(mask,'BendingAngle','Default',0)'./L).^2;
                Kx=K0;
                beta=cat(1,linfoc.beta);
                bx=beta(:,1)';
                by=beta(:,2)';
                alpha=cat(1,linfoc.alpha);
                ax=alpha(:,1)';
                ay=alpha(:,2)';
                mu=cat(1,linfoc.mu);
                fx=mu(:,1)';
                fy=mu(:,2)';
                disp=cat(2,linfoc.Dispersion);
                dx=disp(1,:);
                dpx=disp(2,:);
                dy=disp(3,:);
                dpy=disp(4,:);
            end
            
            function y=heaviside(x)
                y=sign(x).*(1+signtilde(x))/2;
            end
            
            function y=signtilde(x)
                y=sign(x)-double(x==0);
            end
            
        end
    end
end
