classdef FitHistory < matlab.mixin.Copyable
    %FITHISTORY Stores the history of fit residuals
    
    properties (Access=protected)
        residuals
        atmodels
        labels
    end
    
    methods
        function obj = FitHistory()
            %FITHISTORY	Stores the history of fit residuals

            obj.reset();
        end
        
        function reset(obj)
            obj.residuals=double.empty(0,6);
            obj.atmodels=sr.model.empty();
            obj.labels=string.empty();
        end
        
        function add(obj,atmodel,res,varargin)
            %ADD    Add a new point to the fit history
            label=getargs(varargin,num2str(length(obj.atmodels)+1));
            obj.residuals=[obj.residuals;...
                res.h res.v res.h2v res.v2h res.hdisp res.vdisp];
            obj.atmodels=[obj.atmodels;atmodel.copy()];
            obj.labels=[obj.labels;string(label)];
        end
        
        function varargout=plot(obj,varargin)
            %PLOT   Plot the fit history
            %
            %PLOT(OBJ)
            %PLOT(OBJ,AX)
            %
            %H=PLOT(...)
            
            [ax,varargs]=checkarg(varargin,'generate',@() newaxes('History'));
            npoints=size(obj.residuals,1);
            
            ax.ColorOrder=[0 0 0];
            yyaxis(ax,'left');
            colororder('default');
            left=plot(ax,(1:npoints)',obj.residuals(:,1:4),varargs{:});
            xticks(ax,1:length(obj.labels));
            xticklabels(ax,obj.labels);
            ylabel('Orbit residual [m/rad]');
            grid(ax,'on');
            
            yyaxis(ax,'right');
            colororder('default');
            ax.LineStyleOrder={'--'};
            right=plot(ax,(1:npoints)',obj.residuals(:,5:6),varargs{:});
            ylabel('Dispersion residual [m]');
            legend(ax,'H\rightarrowH','V\rightarrowV','H\rightarrowV',...
                'V\rightarrowH','H dispersion','V dispersion');
            if nargout > 0
                varargout={[left;right]};
            end
        end
        
        function atmodel=get_atmodel(obj,n)
            atmodel=obj.atmodels(n);
        end
        
    end
end
