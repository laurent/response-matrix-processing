classdef SteererVRFit < ORMNewtonFit
    %STEERERVRFIT	Fit Real part of vertical steerer errors
    
    methods
        function obj = SteererVRFit(measurement,reference,varargin)
            %STEERERVRFIT	Fit Real part of vertical steerer errors
            %
            %FIT=STEERERVRFIT(ORM,REFERENCE[,KEYWORD,VALUE])
            %
            % ORM:          Measured response matrix
            % REFERENCE:	Response of the reference lattice
            %
            %  keywords list:
            %
            % 'WorkDir'     Directory for storage of derivatives.
            %               Default: measurement directory
            %
            % 'neigen'      Number of Eigen vectors (default: number of
            %               selected steerers
            %
            % 'select_errors'   Error selection
            %               Default: all
            %
            % All other arguments are used as (property_name, property_value)
            % pairs. This is useful when properties must be set in a well defined order.
            perturb_mask=measurement.select('steerv');
            [select_errors,varargs]=getoption(varargin,'select_errors',measurement.select_sv);
            obj=obj@ORMNewtonFit(measurement,reference,perturb_mask,@SteererVRFit.setkvreal,1.0e-4,...
                'v_cell',@SteererVRFit.vderivs,'select_errors',select_errors,varargs{:});
        end
        
        function [ang,label,tit]=get_errors(obj,atmodel,varargin)
            %GET_ERRORS     Compute V steerer tilt errors
            %
            %TILT=GET_ERRORS(OBJ,ATMODEL[,KEYWORD,VALUE])
            %   Compute the V steerer tilt errors for the fitted ATMODEL ring
            % 
            % ATMODEL:  Test lattice
            ang=angle(atmodel.getfieldvalue(obj.perturb_mask,'AxesDef',{2},'Default',1i))-pi/2;
            label='Tilt angle [rd]';
            tit='Vertical steerer angle';
        end
        
    end
    
    methods (Access=protected)
        
        function errors_set=set_errors(obj,desired_mask)
            % Set the default number of eigen vectors to the number of
            % errors (Maximum possible)
            errors_set=set_errors@ORMNewtonFit(obj,desired_mask);
            obj.neigen=sum(errors_set);
        end
        
    end
    
    methods (Access=protected, Static)

        function elem=setkvreal(elem,value)
            %SETKVREAL	Apply real vertical perturbation
             increment=[0 value];
           try
                scale=elem.AxesDef;
            catch
                scale=[1 1i];
            end
            elem.AxesDef=scale + increment;
        end
        
    end
    
    methods (Static)
       
        % Functions for computing the derivatives.
        
        function derivs=vderivs(refmodel,steerers,perturbs,~,~,~)
            %VDERIVS    Compute analytically vertical orbit derivatives
            
            is=reduce_mask(refmodel,steerers,refmodel.select_sh);
            hcells=refmodel.hcells(is);
            
            derivs(1:length(steerers),1:length(perturbs))={zeros(320,1)};
            for i=1:length(steerers)
                derivs(i,perturbs==steerers(i))=hcells(i);
            end
        end
        
    end
    
end
