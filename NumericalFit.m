classdef NumericalFit < ORMNewtonFit
    %NumericalFit     General purpose numerical fit
    %
    %   NumericalFit provides numerical derivation using findsyncorbit
    
    methods
        function obj = NumericalFit(measurement,reference,perturb_mask,perturb_fun,perturb_step,varargin)
            %ORMNEWTONFIT	General purpose Raphson-Newton fit
            %
            %FIT=ORMNEWTONFIT(ORM,REFERENCE,PERTURB_MASK,PERTURB_FUNC,PERTURB_STEP[,KEYWORD,VALUE])
            %
            % ORM:          Measured response matrix
            % REFERENCE:	Response of the reference lattice
            % PERTURB_MASK: Location of all elements where pertubations may be applied
            % PERTURB_FUNC: Function to apply a pertubation to elements marked in PERTURB_MASK
            % PERTURB_STEP: Perturbation step to apply for computing derivatives
            %
            %  keywords list
            %
            % 'WorkDir'     Directory for storage of derivatives.
            %               Default: measurement directory
            %
            % 'neigen'      Number of Eigen vectors
            %
            % 'select_errors'   Error selection
            %               Default: all
            %
            % 'h_cell'      Function computing a single derivative of the response to H steerers
            % 'h_savecell'      "
            % 'h_block'     	"
            % 'h_saveblock'     "
            %               The keyword indicates the kind of storage used
            %               for the derivatives:
            %       cell        Each cell is computed on the 1st time it is referenced
            %       savecell    Identical but the cell array is backed-up on disk
            %                   after each modification
            %       block       blocks of cells are computed on the 1st time they are referenced
            %       saveblock   Identical but the cell array is backed-up on disk
            %                   after each modification
            %
            % 'v_cell'      Function computing a single derivative of the response to V steerers
            % 'v_savecell'      "
            % 'v_block'     	"
            % 'v_saveblock'     "
            %
            % 'f_cell'      Function computing a single derivative of the response to frequency
            % 'f_savecell'      "
            % 'f_block'     	"
            % 'f_saveblock'     "
            %
            % 'tune_cell'	Function computing a single derivative of the tunes
            % 'tune_savecell'	"
            % 'tune_block'      "
            % 'tune_saveblock'	"
            
            % All other arguments are used as (property_name, property_value)
            % pairs. This is useful when properties must be set in a well defined order.
            
            obj=obj@ORMNewtonFit(measurement,reference,perturb_mask,perturb_fun,perturb_step,...
                'h_savecell',@NumericalFit.hderivs,'v_savecell',@NumericalFit.vderivs,...
                'f_savecell',@NumericalFit.fderivs,'tune_savecell',@NumericalFit.tderivs,...
                varargin{:});
                        
        end
        
    end
        
    methods (Static)
        
        % Functions for computing the derivatives. These functions should
        % be compiled / parallelized for efficiency
        
        
        function derivs=hderivs(refmodel,steerers,perturbs,func,step,progress)
            %HDERIVS    Compute numerically horizontal orbit derivatives
            %
            % DERIV=HDERIVS(REFMODEL,STEERERS,PERTURBS,FUNC,STEP,PROGRESS)
            %
            % REFMODEL: Theoretical response matrix
            % STEERERS: list of steerers indexes in the ring
            % PERTURBS: list of perturbation indexes in the ring
            % FUNC:     Perturbation function
            % STEP:     Perturbation step for computing the derivative
            % PROGRESS: Progress indicator function
            
            atmodel=refmodel.atmodel;
            % Initial response
            is=reduce_mask(atmodel,steerers,refmodel.select_sh);
            hcells=refmodel.hcells(is);
            
            % Loop on perturbations
            progress(length(perturbs),'Computing H response');
            cf=arrayfun(@a,perturbs,'UniformOutput',false);
            
            % Reshape the result
            derivs=cat(1,cf{:})';
            
            function resp=a(iq)
                elsave=atmodel.ring{iq};                        % Save 1 element
                atmodel.substitute(iq,@(elem) func(elem,step)); % Apply perturbation
                m=OrbitResponse(atmodel,refmodel.ct,'select_sh',steerers,'progress',@noprogress); % Loop on steerers
                resp=cellfun(@(rsp,rsp0) (rsp-rsp0)./step,m.hcells,hcells,'UniformOutput',false);
                atmodel.substitute(iq,@(elem) elsave);          % Restore the element
                progress();
            end
            
        end
        
        function derivs=vderivs(refmodel,steerers,perturbs,func,step,progress)
            %VDERIVS    Compute numerically vertical orbit derivatives
            %
            % DERIV=VDERIVS(REFMODEL,STEERERS,PERTURBS,FUNC,STEP,PROGRESS)
            %
            % REFMODEL: Theoretical response matrix
            % STEERERS: list of steerers indexes in the ring
            % PERTURBS: list of perturbation indexes in the ring
            % FUNC:     Perturbation function
            % STEP:     Perturbation step for computing the derivative
            % PROGRESS: Progress indicator function
            
            atmodel=refmodel.atmodel;
            % Initial response
            is=reduce_mask(atmodel,steerers,refmodel.select_sv);
            vcells=refmodel.vcells(is);
            
            % Loop on perturbations
            progress(length(perturbs),'Computing V response');
            cf=arrayfun(@a,perturbs,'UniformOutput',false);
            
            % Reshape the result
            derivs=cat(1,cf{:})';
            
            function resp=a(iq)
                elsave=atmodel.ring{iq};                        % Save the element
                atmodel.substitute(iq,@(elem) func(elem,step)); % Apply perturbation
                m=OrbitResponse(atmodel,refmodel.ct,'select_sv',steerers,'progress',@noprogress);	% Loop on steerers
                resp=cellfun(@(rsp,rsp0) (rsp-rsp0)./step,m.vcells,vcells,'UniformOutput',false);
                atmodel.substitute(iq,@(elem) elsave);          % Restore the element
                progress();
            end
            
        end
        
        function derivs=fderivs(refmodel,~,perturbs,func,step,progress)
            %FDERIVS    Compute numerically frequency response derivatives
            %
            % DERIV=FDERIVS(REFMODEL,~,PERTURBS,FUNC,STEP,PROGRESS)
            %
            % REFMODEL: Theoretical response matrix
            % PERTURBS: list of perturbation indexes in the ring
            % FUNC:     Perturbation function
            % STEP:     Perturbation step for computing the derivative
            % PROGRESS: Progress indicator function

            atmodel=refmodel.atmodel;
            % Loop on perturbations
            progress(length(perturbs),'Computing frequency response');
            derivs=arrayfun(@a,perturbs,'UniformOutput',false);
            
            function resp=a(iq)
                elsave=atmodel.ring{iq};                        % Save the element
                atmodel.substitute(iq,@(elem) func(elem,step)); % Apply perturbation
                m=OrbitResponse(atmodel,refmodel.ct,'disp_planes',1:2,'progress',@noprogress);
                resp=(m.fresp-refmodel.fresp)./step;
                atmodel.substitute(iq,@(elem) elsave);          % Restore the element
                progress();
            end
        end
        
        function derivs=tderivs(refmodel,~,perturbs,func,step,progress)
            %TDERIVS    Compute numerically tune response derivatives
            %
            % DERIV=TDERIVS(REFMODEL,~,PERTURBS,FUNC,STEP,PROGRESS)
            %
            % REFMODEL: Theoretical response matrix
            % PERTURBS: list of perturbation indexes in the ring
            % FUNC:     Perturbation function
            % STEP:     Perturbation step for computing the derivative
            % PROGRESS: Progress indicator function
            
            atmodel=refmodel.atmodel;
            % Loop on perturbations
            progress(length(perturbs),'Computing tune response');
            derivs=arrayfun(@a,perturbs,'UniformOutput',false);
            
            function resp=a(iq)
                elsave=atmodel.ring{iq};                        % Save the element
                atmodel.substitute(iq,@(elem) func(elem,step)); % Apply perturbation
                m=OrbitResponse(atmodel,refmodel.ct,'tune_planes',1:2,'progress',@noprogress);
                resp=(m.tunes-refmodel.tunes)./step;
                atmodel.substitute(iq,@(elem) elsave);          % Restore the element
                progress();
            end
        end
%         
%         function derivs=zero_orbit(~,steerers,perturbs,~,~,~)
%             %ZERO_ORBIT	Compute zero orbit derivatives
%             derivs=repmat({zeros(320,1)},length(steerers),length(perturbs));
%         end
%         
%         function derivs=zero_tunes(~,steerers,perturbs,~,~,~)
%             %ZERO_TUNES Compute zero tune derivatives
%             derivs=repmat({0},length(steerers),length(perturbs));
%         end
    end
end

function noprogress(varargin)
end

