classdef FocFit < ORMNewtonFit & FocusingProps
    
    methods
        function obj = FocFit(measurement,reference,varargin)
            %FOCNUMFIT	Fit of quadrupole strengths
            %
            %FIT=FOCFIT(ORM)
            %
            % ORM:          Measured response matrix
            %
            %FIT=FOCFIT(...[,keyword,value])
            %  keyword is in the following list:
            %
            % 'perturb_fun'	Perturbation function.
            %               FocusingProps.setdkn: use dKn/Kn as variable
            %               FocusingProps.setkn: use dKn as variable (Default)
            %
            % 'WorkDir'     Directory for storage of derivatives.
            %               Default: measurement directory
            %
            % 'select_errors'   Error selection
            %               Default: all
            %
            % All other arguments are used as (property_name, property_value)
            % pairs. This is useful when properties must be set in a well defined order.
            
            [perturb_fun,varargs]=getoption(varargin,'perturb_fun',@FocusingProps.setkn);
            quadmask=measurement.select('qp');
            sfmask=measurement.select('sx');
            dqmask=measurement.select('dq');
            errmask=quadmask | sfmask | dqmask;
            obj=obj@ORMNewtonFit(measurement,reference,errmask,perturb_fun,1.0e-4,...
                'h_cell',@FocFit.hderivs,'v_cell',@FocFit.vderivs,...
                'f_savecell',@NumericalFit.fderivs,'tune_savecell',@NumericalFit.tderivs,...
                varargs{:});
        end
       
    end
    
    methods (Static)
        
        % Functions for computing the derivatives. These functions should
        % be compiled / parallelized for efficiency
        
        function derivs=hderivs(refmodel,steerers,perturbs,~,~,~)
            %HDERIVS    Compute analytically horizontal orbit derivatives
            [~,derivs]=FocFit.hvderivs(refmodel,steerers,perturbs);
        end
        
        function derivs=vderivs(refmodel,steerers,perturbs,~,~,~)
            %VDERIVS    Compute numerically vertical orbit derivatives
            [derivs,~]=FocFit.hvderivs(refmodel,steerers,perturbs);
        end
        
        function [hderivs, vderivs]=hvderivs(orm,shlist,foclist)
            %HVDERIVS   Analytical Response Matrix Derivatives
            %
            % HVDERIVS returns the derivative of the steerers
            % response matrix when changing dipole angles and quadrupole gradients.
            %
            % from loco_fast_ESRFtest.m by Zeus Marti (ALBA CELLS, Barcellona)
            %
            % https://arxiv.org/pdf/1711.06589.pdf
            
            bpmmask=orm.select('bpm');
            
            Qx=orm.getmodelproperty('nuh');
            Qy=orm.getmodelproperty('nuv');
            
            sx=sin(pi*Qx);
            cx=cos(pi*Qx);
            sx2=sin(2*pi*Qx);
            sy=sin(pi*Qy);
            cy=cos(pi*Qy);
            sy2=sin(2*pi*Qy);
            
            nb=sum(bpmmask);
            nc=length(shlist);
            nfq=length(foclist);
            
            linbpm=orm.lindata(bpmmask);
            betab=cat(1,linbpm.beta);
            bxb=betab(:,1);
            byb=betab(:,2);
            mub=cat(1,linbpm.mu);
            fxb=mub(:,1);
            fyb=mub(:,2);
            
            linc=orm.lindata(shlist);
            betac=cat(1,linc.beta);
            bxc=betac(:,1)';
            byc=betac(:,2)';
            muc=cat(1,linc.mu);
            fxc=muc(:,1)';
            fyc=muc(:,2)';
            
            %% take out quad indices
            [Lq,Kxq,Kyq,bxq,byq,axq,ayq,gxq,gyq,fxq,fyq]=optics(orm,foclist);
            %[Lq,Kxq,Kyq,bxq,byq,axq,ayq,gxq,gyq,fxq,fyq]=aveoptics(orm,foclist);
            sKxq=sqrt(Kxq);
            sKyq=sqrt(Kyq);
            sKlxq=sKxq.*Lq;
            sKlyq=sKyq.*Lq;
            
            %% Horizontal response matrix derivative respect to quads
            sin2k=sin(2*sKlxq)./sKxq;
            cos2k=cos(2*sKlxq)-1;
            
            I_k0=(bxq/2+gxq./Kxq/2).*Lq+(bxq/2-gxq./Kxq/2).*sin2k/2+(axq./Kxq/2).*cos2k;
            I_ks2=(-cos2k+(axq./bxq).*(sin2k-2*Lq))./Kxq/2;
            I_kc2=I_k0+(1./bxq./Kxq).*(sin2k-2*Lq)/2;
            
            %terms
            Amplitude_ij=-sqrt(bxb*bxc)/8/sx/sx2;
            ph_ij=fxb*ones(1,nc)-ones(nb,1)*fxc;
            ph_ik=fxb*ones(1,nfq)-ones(nb,1)*fxq;
            ph_jk=fxc'*ones(1,nfq)-ones(nc,1)*fxq;
            
            C_ij1=cos(abs(ph_ij)-pi*Qx);
            S_ij1=signtilde(ph_ij).*sin(abs(ph_ij)-pi*Qx);%
            
            C_ik2=cos(2*abs(ph_ik)-2*pi*Qx);
            S_ik2=signtilde(ph_ik).*sin(2*abs(ph_ik)-2*pi*Qx);
            
            C_jk2=cos(2*abs(ph_jk)-2*pi*Qx);
            S_jk2=signtilde(ph_jk).*sin(2*abs(ph_jk)-2*pi*Qx);
            
            
            Sigma_ik2=(ones(nb,1)*I_kc2).*S_ik2-(ones(nb,1)*I_ks2).*C_ik2;
            Gamma_ik2=(ones(nb,1)*I_ks2).*S_ik2+(ones(nb,1)*I_kc2).*C_ik2;
            Sigma_jk2=(ones(nc,1)*I_kc2).*S_jk2-(ones(nc,1)*I_ks2).*C_jk2;
            Gamma_jk2=(ones(nc,1)*I_ks2).*S_jk2+(ones(nc,1)*I_kc2).*C_jk2;
            
            % the complete thing
            C=Amplitude_ij.*C_ij1;
            S=Amplitude_ij.*S_ij1;
            
            termC1=repmat(permute(Gamma_ik2,[1 3 2]),[1 nc 1]);
            termC2=repmat(permute(Gamma_jk2,[3 1 2]),[nb 1 1]);
            termC3=repmat(permute(2*I_k0*cx^2,[1 3 2]),[nb nc 1]);
            termC=repmat(C,[1 1 nfq]).*(termC1+termC2+termC3);
            
            termS1=repmat(permute(Sigma_ik2+2*sx2*(ones(nb,1)*I_k0).*heaviside(ph_ik),[1 3 2]),[1 nc 1]);
            termS2=repmat(permute(Sigma_jk2+2*sx2*(ones(nc,1)*I_k0).*heaviside(ph_jk),[3 1 2]),[nb 1 1]);
            termS3=repmat(permute(sx2*I_k0,[1 3 2]),[nb nc 1]).*repmat(-signtilde(ph_ij),[1 1 nfq]);
            termS=repmat(S,[1 1 nfq]).*(termS1-termS2+termS3);
            
            hderivs=squeeze(num2cell(termC+termS,1));
            
            %% Vertical response matrix derivative
            sin2k=sin(2*sKlyq)./sKyq;
            cos2k=cos(2*sKlyq)-1;
            
            
            I_k0=(byq/2+gyq./Kyq/2).*Lq+(byq/2-gyq./Kyq/2).*sin2k/2+(ayq./Kyq/2).*cos2k;
            I_ks2=(-cos2k+(ayq./byq).*(sin2k-2*Lq))./Kyq/2;
            I_kc2=I_k0+(1./byq./Kyq).*(sin2k-2*Lq)/2;
            
            %terms
            Amplitude_ij=sqrt(byb*byc)/8/sy/sy2;
            ph_ij=fyb*ones(1,nc)-ones(nb,1)*fyc;
            ph_ik=fyb*ones(1,nfq)-ones(nb,1)*fyq;
            ph_jk=fyc'*ones(1,nfq)-ones(nc,1)*fyq;
            
            C_ij1=cos(abs(ph_ij)-pi*Qy);
            S_ij1=signtilde(ph_ij).*sin(abs(ph_ij)-pi*Qy);%
            
            C_ik2=cos(2*abs(ph_ik)-2*pi*Qy);
            S_ik2=signtilde(ph_ik).*sin(2*abs(ph_ik)-2*pi*Qy);
            
            C_jk2=cos(2*abs(ph_jk)-2*pi*Qy);
            S_jk2=signtilde(ph_jk).*sin(2*abs(ph_jk)-2*pi*Qy);
            
            
            Sigma_ik2=(ones(nb,1)*I_kc2).*S_ik2-(ones(nb,1)*I_ks2).*C_ik2;
            Gamma_ik2=(ones(nb,1)*I_ks2).*S_ik2+(ones(nb,1)*I_kc2).*C_ik2;
            Sigma_jk2=(ones(nc,1)*I_kc2).*S_jk2-(ones(nc,1)*I_ks2).*C_jk2;
            Gamma_jk2=(ones(nc,1)*I_ks2).*S_jk2+(ones(nc,1)*I_kc2).*C_jk2;
            
            % the complete thing
            C=Amplitude_ij.*C_ij1;
            S=Amplitude_ij.*S_ij1;
            
            termC1=repmat(permute(Gamma_ik2,[1 3 2]),[1 nc 1]);
            termC2=repmat(permute(Gamma_jk2,[3 1 2]),[nb 1 1]);
            termC3=repmat(permute(2*I_k0*cy^2,[1 3 2]),[nb nc 1]);
            termC=repmat(C,[1 1 nfq]).*(termC1+termC2+termC3);
            
            termS1=repmat(permute(Sigma_ik2+2*sy2*(ones(nb,1)*I_k0).*heaviside(ph_ik),[1 3 2]),[1 nc 1]);
            termS2=repmat(permute(Sigma_jk2+2*sy2*(ones(nc,1)*I_k0).*heaviside(ph_jk),[3 1 2]),[nb 1 1]);
            termS3=repmat(permute(sy2*I_k0,[1 3 2]),[nb nc 1]).*repmat(-signtilde(ph_ij),[1 1 nfq]);
            termS=repmat(S,[1 1 nfq]).*(termS1-termS2+termS3);
            
            vderivs=squeeze(num2cell(1i*(termC+termS),1));
            
            function [L,Kx,Ky,bx,by,ax,ay,gx,gy,fx,fy]=optics(orm,refpts)
                linfoc=orm.lindata(refpts);
                L=orm.getmodelattribute(refpts,'Length')';
                K0=eps+orm.getmodelattribute(refpts,'PolynomB',{2},'Default',0)';
                %Kx=K0 + (orm.getmodelattribute(mask,'BendingAngle','Default',0)'./L).^2;
                Kx=K0;
                Ky=-K0;
                beta=cat(1,linfoc.beta);
                bx=beta(:,1)';
                by=beta(:,2)';
                alpha=cat(1,linfoc.alpha);
                ax=alpha(:,1)';
                ay=alpha(:,2)';
                gamma=(1+alpha.^2)./beta;
                gx=gamma(:,1)';
                gy=gamma(:,2)';
                mu=cat(1,linfoc.mu);
                fx=mu(:,1)';
                fy=mu(:,2)';
            end
            
            function [L,Kx,Ky,bx,by,ax,ay,gx,gy,fx,fy]=aveoptics(orm,refpts)
                linfoc=orm.avedata(refpts);
                tunes=2*pi*orm.getmodelproperty('tunes');
                L=orm.getmodelattribute(refpts,'Length')';
                K0=eps+orm.getmodelattribute(refpts,'PolynomB',{2},'Default',0)';
                %Kx=K0 + (orm.getmodelattribute(mask,'BendingAngle','Default',0)'./L).^2;
                Kx=K0;
                Ky=-K0;
                bx=linfoc(:,2)';
                by=linfoc(:,3)';
                ax=linfoc(:,3)';
                ay=linfoc(:,4)';
                gx=(1+ax.^2)./bx;
                gy=(1+ay.^2)./by;
                mu=linfoc(:,6:7).*tunes;
                fx=mu(:,1)';
                fy=mu(:,2)';
            end
            
            function y=heaviside(x)
                y=sign(x).*(1+signtilde(x))/2;
            end
            
            function y=signtilde(x)
                y=sign(x)-double(x==0);
            end
            
        end
    end
end
