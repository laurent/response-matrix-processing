classdef SkewNumFit < NumericalFit & CouplingProps

    methods
        function obj = SkewNumFit(measurement,reference,varargin)
            %SKEWNUMFIT     Fit of skew quadrupole strengths
            %
            %FIT=SKEWNUMFIT(ORM,REFERENCE)
            %
            % ORM:          Measured response matrix
            % REFERENCE:	Response of the reference lattice
            %
            %FIT=SKEWNUMFIT(...[,keyword,value])
            %  keyword is in the following list:
            %
            % 'perturb_fun'	Perturbation function.
            %               @CouplingProps.setdks: use dKs/Kn as variable
            %               @CouplingProps.setks: use dKs as variable (Default)
            %
            % 'WorkDir'     Directory for storage of derivatives.
            %               Default: measurement directory
            %
            % 'select_errors'   Error selection
            %               Default: all
            %
            % All other arguments are used as (property_name, property_value)
            % pairs. This is useful when properties must be set in a well defined order.
            
            [perturb_fun,varargs]=getoption(varargin,'perturb_fun',@CouplingProps.setks);
            quadmask=measurement.select('qp');
            sfmask=measurement.select('sx');
            dqmask=measurement.select('dq');
            errmask=quadmask | sfmask | dqmask;
            obj=obj@NumericalFit(measurement,reference,errmask,perturb_fun,...
                1.0e-4,varargs{:});
        end
        
    end
end
