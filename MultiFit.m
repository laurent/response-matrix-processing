classdef MultiFit < ParameterNewtonFit

    properties
        fitlist
    end
    
    methods
        
        function obj=MultiFit(measurement,varargin)
            %MULTIFIT   Container for multiple ParameterNewtonFit objects
            %
            %MULTIFIT(MEASUREMENT) creates an empty  MULTIFIT object
            %
            %MULTIFIT(...[,keyword,value])
            %  keyword is in he following list:
            %
            % 'select_sh'	Horizontal orbits selected for the fit
            %               Default: all measured orbits
            %
            % 'select_sv'	vertical orbits selected for the fit
            %               Default: select_sh
            %
            % 'select_sh2v'	Horizontal orbits selected for the fit
            %               Default: select_sh
            %
            % 'select_sv2h'	Vertical orbits selected for the fit
            %               Default: select_sv
            %
            % 'disp_planes'	Dispersion planes selected for the fit (1:H, 2:V)
            %               Default: 1:2
            %
            % 'tunes_planes'Tunes selected for the fit (1:H, 2:V)
            %               Default: 1:2
            %
            % All other arguments are used as (property_name, property_value) pairs.
            % This is useful when properties must be set in a well defined order.
            
            obj=obj@ParameterNewtonFit(measurement,[],@(elem,value) elem,...
                'fitlist',struct('fit',{},'weight',{}),varargin{:});
        end
        
        function add_fit(obj,fit,weight)
            %MULTIFIT.ADD_FIT(PNFIT)    Add a ParameterNewtonFit object to
            %                           the multifit object
            %
            % PNFIT:    ParameterNewtonFit object. The present
            %           perturbation values of PNFIT are kept
            
            if ~isa(fit,'ParameterNewtonFit')
                error('wrong parameter');
            end
            if nargin < 3
                weight = 1;
            end
            
            % Store the newly added fit object
            n=length(obj.fitlist)+1;
            obj.fitlist(n)=struct('fit',fit,'weight',weight);
            
            % Set compatible properties to the newly added fit
            fit.select_sh=obj.select_sh2v;
            fit.select_sh2v=obj.select_sh;
            fit.select_sv2h=obj.select_sv2h;
            fit.select_sv=obj.select_sv;
            fit.disp_planes=obj.disp_planes;
            fit.tune_planes=obj.tune_planes;
            fit.orbit_weight=obj.orbit_weight;
            fit.disp_weight=obj.disp_weight;
            fit.tune_weight=obj.tune_weight;
            fit.bpm_set_=obj.bpm_set_;
        end
            
        function atmodel=apply_perturbations(obj,atmodel,perturb_values)
            fitcell={obj.fitlist.fit};  % Cell array of fit objects
            split=cellfun(@(fitn) sum(fitn.errors_set_),fitcell);
            increment=mat2cell(perturb_values,split,1);
            for i=1:length(obj.fitlist)
                atmodel=apply_perturbations(obj.fitlist(i).fit,atmodel,increment{i});
            end
        end
        
        function lhs=assemble(obj)
            blocks=arrayfun(@(s) s.weight*assemble(s.fit), obj.fitlist,'UniformOutput',false);
            lhs=cat(2,blocks{:});
        end
    end
    
    methods (Access=protected, Hidden)
        
        % Property access methods. The property value is transmitted to all
        % embedded fit objects
        
        function set_sh(obj,steerers)
            set_sh@ParameterNewtonFit(obj,steerers);
            cellfun(@(fitn) set_sh(fitn,steerers),{obj.fitlist.fit});
        end
        function set_sh2v(obj,steerers)
            set_sh2v@ParameterNewtonFit(obj,steerers);
            cellfun(@(fitn) set_sh2v(fitn,steerers),{obj.fitlist.fit});
        end
        function set_sv2h(obj,steerers)
            set_sv2h@ParameterNewtonFit(obj,steerers);
            cellfun(@(fitn) set_sv2h(fitn,steerers),{obj.fitlist.fit});
        end
        function set_sv(obj,steerers)
            set_sv@ParameterNewtonFit(obj,steerers);
            cellfun(@(fitn) set_sv(fitn,steerers),{obj.fitlist.fit});
        end
        function set_disp_planes(obj,~)
            cellfun(@splane,{obj.fitlist.fit});
            function splane(fit)
                fit.disp_planes=obj.disp_planes;
            end
        end
        function set_tune_planes(obj,~)
            cellfun(@splane,{obj.fitlist.fit});
            function splane(fit)
                fit.tune_planes=obj.tune_planes;
            end
        end
        
    end
    
end
