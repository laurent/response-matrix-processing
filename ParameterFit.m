classdef ParameterFit < handle
    %PARAMETERFIT is the minimum information set to perform a parameter fit
    %
    %   It provides:
    %   - a set of variables (elements if the AT structure)
    %   - a perturbation function to be applied to variable elements
    %
    
    % Public properties: locations are expressed as logical masks or
    % list of integers, refering to the whole ring (like "refpts")
    
    properties (Constant)   % A single history buffer is shared by all
        history=FitHistory()% ParameterFit objects
    end
    properties (SetAccess=immutable)
        measurement         % Measured response
        perturb_fun         % Element perturbation function
        perturb_mask        % Location of all potential perturbations
    end
    properties
        neigen=96           % Number of Singular values to be used in solver
        working_directory   % For file storage
    end
    properties (Dependent)
        select_errors       % Actually used perturbations
    end
    
    % Private properties: locations are expressed as logical masks on
    % the subset of corresponding elements.
    properties (Access=protected)
        errors_set_         % Subset of errors          [n_errors, 1]
    end
    properties (Constant, Access=protected, Hidden)
        fold_args={}
    end
    
    methods
        
        function obj = ParameterFit(measurement,perturb_mask,perturb_fun,varargin)
            %PARAMETERFIT   Abstract base class for parameter fit
            %
            %FIT=PARAMETERFIT(ORM,PERTURB_MASK,PERTURB_FUNC)
            %
            % ORM:          Measured response matrix
            % PERTURB_MASK: Location of all elements where pertubations may be applied
            % PERTURB_FUNC: Function to apply a pertubation to elements marked in ERROR_MASK
            %
            %FIT=PARAMETERFIT(...[,keyword,value])
            %  keyword is in the following list:
            %
            % 'WorkDir'     Directory for storage of derivatives.
            %               Default: measurement directory
            %
            % 'select_errors'   Error selection
            %               Default: all
            
            % All other arguments are used as (property_name, property_value)
            % pairs. This is useful when properties must be set in a well defined order.
            
            if nargin > 0
                obj.measurement=measurement;
                obj.perturb_mask=logical_mask(perturb_mask,size(measurement.atmodel.ring));
                obj.perturb_fun=perturb_fun;
                
                default_wd=fullfile(measurement.location,'derivatives');
                [obj.working_directory,varargs]=getoption(varargin,'WorkDir',default_wd);
                [obj.select_errors,varargs]=getoption(varargs,'select_errors',obj.perturb_mask);
                keywords=varargs(1:2:end);
                values=varargs(2:2:end);
                
                % Use remaining arguments as (property_name, property_value) pairs
                cellfun(@setprop,keywords,values);
                
                % Check existence of working directory
                if ~exist(obj.working_directory,'file')
                    [parent,name, ~]=fileparts(obj.working_directory);
                    [success,message,messageid]=mkdir(parent,name);
                    if ~success
                        error(messageid,['Cannot create working directory: ' message]);
                    elseif ~isempty(messageid)
                        warning(messageid,message);
                    end
                end
            end
            
            function setprop(key,val)
                obj.(key)=val;
            end
        end
        
        function varargout=bar(obj,varargin)
            %BAR	Bar plot of errors
            %
            %BAR(OBJ,ATMODEL,[,KEYWORD,VALUE])
            %   Plot errors for a given AT model
            %BAR(OBJ,AXES,ATMODEL[,KEYWORD,VALUE])
            %   Plot errors int the given axes
            %
            % ATMODEL:  Test lattice
            %
            %   keywords controlling the layout (one of those):
            %
            % 'group'   Group errors by family (Default)
            % 'flat'    Plot all errors along the ring
            % 'std'     Plot the standard deviation of errors
            % 'mean'    Plot the average error value
            %
            %   keywords controlling the symmetrization of the cell
            %
            % 'sym'     Use the symmetry of the lattice
            
            foldargs=obj.fold_args;
            [ax,varargs]=checkarg(varargin);
            [atmodel,varargs]=checkarg(varargs,'check',@(arg) true);
            [sym,varargs]=getflag(varargs,'sym');
            [pmean,varargs]=getflag(varargs,'mean');
            [pstd,varargs]=getflag(varargs,'std');
            [pflat,varargs]=getflag(varargs,'flat');
            if ~sym, foldargs=[foldargs 'nosym']; end
            [errs,ylab,tit]=get_errors(obj,atmodel,varargs{:});
            if pflat
                bar(ax,errs);
                xlabel('Error #');
            else
                [errors,labels]=obj.fold_errors(errs,foldargs{:},varargs{:});
                if pmean
                    [varargout{1:nargout}]=bar(ax,mean(errors,2,'omitnan'));
                elseif pstd
                    [varargout{1:nargout}]=bar(ax,std(errors,1,2,'omitnan'));
                else
                    [varargout{1:nargout}]=bar(ax,errors);
                end
                xticks(ax,1:length(labels));
                xticklabels(ax,labels);
                xtickangle(45);
            end
            ylabel(ax,ylab);
            title(ax,tit);
            grid(ax,'on');
        end
        
        % Property access functionsax,errs
        
        function varmask=get.select_errors(obj)
            varmask=obj.perturb_mask;
            varmask(varmask)=obj.errors_set_;
        end
        
        % Matlab does not allow overloading property setters. So we delegate
        % the action to a standard method which can be overloaded
        
        function set.select_errors(obj,desired_mask)
            obj.errors_set_=obj.set_errors(desired_mask);
        end
        
    end
    
    methods (Abstract)
        [errors,label,tit]=get_errors(obj,atmodel,varargin);
    end
    
    methods (Static)
        
        function residuals(measurement,fitted,varargin)
            %RESIDUALS  Display and store the state of the fit
            %
            %RESIDUALS(MEASUREMENT,FITTED)
            %   Display the orbit and dispersion residuals
            %
            %RESIDUALS(MEASUREMENT,FITTED,LABEL)
            %   Display the orbit and dispersion residuals and add the point
            %   to the history
            
            label=getargs(varargin,{[]});   % empty array must be enclosed
            res=residuals(measurement-fitted);
            if ~isempty(label)
                ParameterFit.history.add(fitted.atmodel,res,label);
            end
        end
    end
    
    methods (Access=protected)
        
        function [errors,labels]=fold_errors(obj,errs,varargin)
            %FOLD_ERRORS    Apply the symmetries of the lattice
            %errors=sr.fold(errs,varargin{:});
            errors=srfoldtempo(errs,varargin{:});       % Until matlabforoperation is updated
            labels=obj.foldname(numel(errs),varargin{:});
        end
        
        % Property access methods
        function errors_set=set_errors(obj,desired_mask)
            errors_set=reduce_mask(obj.measurement,desired_mask,obj.perturb_mask);
        end
    end
    
    methods (Access=protected, Static)
        
        function dq=svdsolve(lhs,rhs,neig)
            %SVDSOLVE  SVD solver
            
            [u,s,v]=svd(lhs,'econ');
            lambda=diag(s);
            nmax=length(lambda);
            if neig > nmax
                neig=nmax;
                warning('Svd:maxsize',['number of vectors limited to ' num2str(nmax)]);
            end
            eigsorb=u'*rhs;
            eigscor=eigsorb(1:neig)./lambda(1:neig);
            dq=v(:,1:neig)*eigscor;
            %
            %             Amod = u(:,1:neig) * s(1:neig,1:neig);
            %
            %             err = v(:,1:neig)*(Amod'*Amod)\v(:,1:neig)';
            %
            %             st = s;
            %             st([(neig+1):end;(neig+1):end])=0; %zero eigenvalues above cut
            %             R = u*st*v'; %truncated pseudo inverse
            %             dx = (lhs - R*dq);
            %
            %             % dq_err = Rinv * dx; %repmat(dx,size(b));
            %             dq_err = sqrt(diag(err)*mean(dx.^2))/200;
        end
        
        function labels=foldname(nb,varargin)
            sym=~getflag(varargin,'nosym');
            if sym
                labs={'qf1ae','sh1/3','qd2ae','qd3ae','sd1ae','qf4ae',...
                'sf2ae','qf4bd','sd1bd','qd5bd','qf6bd','dq1bd','qf8bd',...
                'sh2','dq2c','qf1inj','sh1/3inj','qd2inj','qf2inj','sd1ainj',...
                'sf2ainj','sd1binj','sd1dinj','sf2einj','sd1einj'};
            else
                labs={'qf1a','sh1','qd2a','qd3a','sd1a','qf4a','sf2a',...
                'qf4b','sd1b','qd5b','qf6b','dq1b','qf8b','sh2','dq2c',...
                'qf8d','dq1d','qf6d','qd5d','sd1d','qf4d','sf2e','qf4e',...
                'sd1e','qf3e','qd2e','sh3','qf1e'};
            end
            sel=false(size(labs));

            if nb == 802            % All magnets (802)
                if sym
                    sel([1 3:13 15 16 18:25])=true;
                else
                    sel([1 3:13 15:26 28])=true;
                end
            elseif nb == 514        % Quadrupoles (514)
                if sym
                    sel([1 3:4 6 8 10:11 13 16 18:19])=true;
                else
                    sel([1 3:4 6 8 10:11 13 16 18:19 21 23 26:26 28])=true;
                end
            elseif nb == 384        % H steerers (384)  Ignore DQs
                if sym
%                   sel([2 5 7 9 12 14 15 17 20 21])=true;
                    sel([2 5 7 9 14 17 20 21])=true;
                else
%                   sel([2 5 7 9 12 14 15 17 20 22 24 27])=true;
                    sel([2 5 7 9 14 20 22 24 27])=true;
                end
            elseif nb == 288        % V steerers (288)
                if sym
                    sel([2 5 7 9 14 17 20 21])=true;
                else
                    sel([2 5 7 9 14 20 22 24 27])=true;
                end
            elseif nb == 192        % Sextupoles (192)
                if sym
                    sel([5 7 9 20:25])=true;
                else
                    sel([5 7 9 20 22 24])=true;
                end
            elseif nb == 96         % DQs (96)
                if sym
                    sel([12 15])=true;
                else
                    sel([12 15 17])=true;
                end
            elseif nb == 320        % BPMs (320)
                if sym
                    labs={'BPM1/10','BPM2/9','BPM3/8','BPM4/7','BPM5/6'};
                else
                    labs={'BPM1','BPM2','BPM3','BPM4','BPM5','BPM6','BPM7','BPM8','BPM9','BPM10'};
                end
                sel=true(size(labs));
            end
            labels=labs(sel);
        end
        
    end
end

