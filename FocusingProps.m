classdef (Abstract) FocusingProps < ParameterFit
%FocusingProps  Provide error processing for focusing errors
    
    methods
        
        function [errs,ylabel,tit]=get_errors(obj,atmodel,varargin)
            %GET_ERRORS     Compute focusing errors
            %
            %DKK=GET_ERRORS(OBJ,ATMODEL[,KEYWORD,VALUE])
            %   Compute the focusing errors DK/K for the fitted ATMODEL ring
            % 
            % ATMODEL:  Test lattice
            %
            %DK=GET_ERRORS(...,'absolute')
            %   Return the absolute error DKn
            
            [absolute,varargs]=getflag(varargin,'absolute'); %#ok<ASGLU>
            kn=atmodel.getfieldvalue(obj.perturb_mask,'PolynomB',{2});
            k0=obj.measurement.atmodel.getfieldvalue(obj.perturb_mask,'PolynomB',{2});
            if absolute
                errs=kn-k0;
                ylabel='\DeltaK_n [m^{-2}]';
            else
                ks=atmodel.getfieldvalue(obj.perturb_mask,'PolynomA',{2});
                [k,~]=compute_tilt(kn,ks);
                errs=(k-k0)./k0;
                ylabel='\Deltak/k';
            end
            tit='Focusing errors';
            
            function [kl, tilt]=compute_tilt(klnorm,klskew)
                oknorm=(klnorm~=0);
                kl=klskew;
                tilt=-pi/4*(klskew~=0);
                ratio=klskew(oknorm)./klnorm(oknorm);
                kl(oknorm)=klnorm(oknorm).*sqrt(1+ratio.*ratio);
                tilt(oknorm)=-0.5*atan(ratio);
            end
        end
        
    end
    
    methods (Access=protected, Sealed)
        
        function [errors,labels]=fold_errors(obj,errs,varargin)
            [absolute,varargs]=getflag(varargin,'absolute');
            if absolute
                [errors,labels]=fold_errors@ParameterFit(obj,errs,varargs{:});
                if ~getflag(varargs,'nosym')
                    errors=errors(1:13,:);
                    labels=labels(1:13);
                end
            else
                quadmask=obj.measurement.select('qp');
                dqmask=obj.measurement.select('dq');
                % Extract quad errors
                qperrs=errs(quadmask(obj.perturb_mask));
                [qperrors,qplabels]=fold_errors@ParameterFit(obj,qperrs,varargs{:});
                % Extract dq errors
                dqerrs=errs(dqmask(obj.perturb_mask));
                % Join errors
                [dqerrors,dqlabels]=fold_errors@ParameterFit(obj,dqerrs,varargs{:});
                if getflag(varargs,'nosym')
                    errors=[qperrors;dqerrors];
                    labels=[qplabels dqlabels];
                else
                    errors=[qperrors(1:8,:);dqerrors];
                    labels=[qplabels(1:8) dqlabels];
                end
            end
        end
        
    end
    
    methods (Static)
        function elem=setkn(elem,value)
            %SETKN  Use DeltaKn as variable
            elem.PolynomB(2)=elem.PolynomB(2) + value;
        end
        function elem=setdkn(elem,value)
            %SETKDN  Use DeltaKn/Kn as variable
            elem.PolynomB(2)=elem.PolynomB(2)*(1.0+value);
        end
    end
    
end

