classdef BpmVIFit < ORMNewtonFit
    %BPMVIFIT	Fit Imaginary part of vertical BPM errors
    
    methods
        function obj = BpmVIFit(measurement,reference,varargin)
            %BPMVIFIT	Fit Imaginary part of vertical BPM errors
            %
            %FIT=BPMVIFIT(ORM,REFERENCE[,KEYWORD,VALUE])
            %
            % ORM:          Measured response matrix
            % REFERENCE:	Response of the reference lattice
            %
            %  keywords list:
            %
            % 'WorkDir'     Directory for storage of derivatives.
            %               Default: measurement directory
            %
            % 'neigen'      Number of Eigen vectors (default: number of
            %               selected bpms
            %
            % 'select_errors'   Error selection
            %               Default: all
            %
            % All other arguments are used as (property_name, property_value)
            % pairs. This is useful when properties must be set in a well defined order.
            
            perturb_mask=measurement.select('bpm');
            [select_errors,varargs]=getoption(varargin,'select_serrors',perturb_mask);
            obj=obj@ORMNewtonFit(measurement,reference,perturb_mask,@BpmVIFit.setbvimag,1.0e-4,...
                'v_cell',@BpmVIFit.vderivs,'select_errors',select_errors,...
                'sum_weight',10,varargs{:});
        end
        
        function [scale,label,tit]=get_errors(obj,atmodel,varargin)
            %GET_ERRORS     Compute V BPM scaling errors
            %
            %SCALE=GET_ERRORS(OBJ,ATMODEL[,KEYWORD,VALUE])
            %   Compute the V BPM scaling errors for the fitted ATMODEL ring
            % 
            % ATMODEL:  Test lattice
            scale=abs(atmodel.getfieldvalue(obj.perturb_mask,'AxesDef',{2},'Default',1i))-1;
            label='\Deltaz/z';
            tit='Vertical BPM scaling';
        end
        
    end
    
    methods (Access=protected)
        
        function errors_set=set_errors(obj,desired_mask)
            % Set the default number of eigen vectors to the number of
            % errors (Maximum possible)
            errors_set=set_errors@ORMNewtonFit(obj,desired_mask);
            obj.neigen=sum(errors_set);
        end
        
    end
    
    methods (Access=protected, Static)

        function elem=setbvimag(elem,value)
            %SETBVIMAG	Apply imaginary vertical perturbation
            increment=[0 1i*value];
            try
                scale=elem.AxesDef;
            catch
                scale=[1 1i];
            end
            elem.AxesDef=scale + increment;
        end
        
    end
    
    methods (Static)
       
        % Functions for computing the derivatives.
        
        function derivs=vderivs(refmodel,steerers,perturbs,~,~,~)
            %VDERIVS	Compute analytically vertical orbit derivatives
            
            iq=find(reduce_mask(refmodel,perturbs,refmodel.select('bpm')));
            is=find(reduce_mask(refmodel,steerers,refmodel.select_sh));
            [jq,js]=meshgrid(iq,is);
            derivs=arrayfun(@gener,js,jq,'UniformOutput',false);
            
            function resp=gener(js,jq)
                resp=zeros(320,1);
                resp(jq)=-refmodel.vcells{js}(jq);
            end
        end
        
    end
    
end
