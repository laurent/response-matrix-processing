classdef SteererHRFit < ORMNewtonFit
    %STEERERHRFIT	Fit Real part of horizontal steerer errors
    
    properties (Constant, Access=protected, Hidden)
        fold_code={'hst'}       % Code for sr.fold
    end
    
    methods
        function obj = SteererHRFit(measurement,reference,varargin)
            %STEERERHRFIT	Fit Real part of horizontal steerer errors
            %
            %FIT=STEERERHRFIT(ORM,REFERENCE[,KEYWORD,VALUE])
            %
            % ORM:          Measured response matrix
            % REFERENCE:	Response of the reference lattice
            %
            %  keywords list:
            %
            % 'WorkDir'     Directory for storage of derivatives.
            %               Default: measurement directory
            %
            % 'neigen'      Number of Eigen vectors (default: number of
            %               selected steerers
            %
            % 'select_errors'   Error selection
            %               Default: all
            %
            % All other arguments are used as (property_name, property_value)
            % pairs. This is useful when properties must be set in a well defined order.
            perturb_mask=measurement.select('steerhdq');
            [select_errors,varargs]=getoption(varargin,'select_errors',measurement.select_sh);
            obj=obj@ORMNewtonFit(measurement,reference,perturb_mask,@SteererHRFit.setkhreal,1.0e-4,...
                'h_cell',@SteererHRFit.hderivs,'select_errors',select_errors,varargs{:});
        end
        
        function [scale,label,tit]=get_errors(obj,atmodel,varargin)
            %GET_ERRORS     Compute H steerer scaling errors
            %
            %SCALE=GET_ERRORS(OBJ,ATMODEL[,KEYWORD,VALUE])
            %   Compute the H steerer scaling errors for the fitted ATMODEL ring
            % 
            % ATMODEL:  Test lattice
            scale=abs(atmodel.getfieldvalue(obj.perturb_mask,'AxesDef',{1},'Default',1))-1;
            label='\Delta\theta_H/\theta_H';
            tit='Horizontal steerer scaling';
        end
        
    end
    
    methods (Access=protected)
        
        function errors_set=set_errors(obj,desired_mask)
            % Set the default number of eigen vectors to the number of
            % errors (Maximum possible)
            errors_set=set_errors@ORMNewtonFit(obj,desired_mask);
            obj.neigen=sum(errors_set);
        end
        
    end
    
    methods (Access=protected, Static)

        function elem=setkhreal(elem,value)
            %SETKHREAL	Apply real horizontal perturbation
            increment=[value 0];
            try
                scale=elem.AxesDef;
            catch
                scale=[1 1i];
            end
            elem.AxesDef=scale + increment;
        end
        
    end
    
    methods (Static)
       
        % Functions for computing the derivatives.
        
        function derivs=hderivs(refmodel,steerers,perturbs,~,~,~)
            %HDERIVS    Compute analytically horizontal orbit derivatives
            
            is=reduce_mask(refmodel,steerers,refmodel.select_sh);
            hcells=refmodel.hcells(is);
            
            derivs(1:length(steerers),1:length(perturbs))={zeros(320,1)};
            for i=1:length(steerers)
                derivs(i,perturbs==steerers(i))=hcells(i);
            end
        end
        
    end
    
end
