global MACHFS

orm=OrbitResponse(fullfile(MACHFS,'MDT/2020/2020_02_29/resp2')); orm.bpm_ignorelist=31;

orm=OrbitResponse(fullfile(MACHFS,'MDT/2020/2020_06_07/resp2'));

orm=OrbitResponse(fullfile(MACHFS,'MDT/2020/2020_06_27/resp2'));
orm=OrbitResponse(fullfile(MACHFS,'MDT/2020/2020_06_27/resp4'));

%% Selection of fitted elements
reference=orm.get_reference();
qd3mask=orm.select('QD3[ABDEIJ]');
qf4mask=orm.select('QF4[ABDEIJ]');
qd5mask=orm.select('QD5[ABDEIJ]');
sd1mask=orm.select('S[DIJ]1.*');
sf2mask=orm.select('S[FIJ]2.*');
selmask=qd3mask | qf4mask | qd5mask;
quadmask=qd3mask | sf2mask | qd5mask;
skewmask=sd1mask | sf2mask;

%% Creation of "Fit" objects
% Magnet fit objects
fnumquad=FocNumFit(orm,reference,'select_errors',selmask);
fquad=FocFit(orm,reference,'select_errors',selmask);
fnumskew=SkewNumFit(orm,reference,'select_errors',selmask);
fskew=SkewFit(orm,'select_errors',selmask);

% Steerer fit objects
fithscale=SteererHRFit(orm,reference);
fithrot=SteererHIFit(orm,reference);
fitvrot=SteererVRFit(orm,reference);
fitvscale=SteererVIFit(orm,reference);
fsteerers=[fithscale fithrot fitvrot fitvscale];

% BPM fit objects
fbgh=BpmHRFit(orm,reference);
fbrh=BpmHIFit(orm,reference);
fbrv=BpmVRFit(orm,reference);
fbgv=BpmVIFit(orm,reference);
fbpms=[fbgh fbrh fbrv fbgv];

%% (Re)initialisation of the fit
ParameterFit.history.reset();   % reset history
fitted=reference.copy();        % reset fitted model
ParameterFit.residuals(orm,fitted,'Initial');

%% Sequential fit
fitted=fit(fsteerers,fitted);
ParameterFit.residuals(orm,fitted,'Steerers');

fitted=fit([fnumquad fnumskew],fitted);
ParameterFit.residuals(orm,fitted,'Quads+Skews');

fitted=fit([fbpms fsteerers],fitted);
ParameterFit.residuals(orm,fitted,'Bpms+Steerers');

fitted=fit([fnumquad fnumskew],fitted);
ParameterFit.residuals(orm,fitted,'Quads+Skews');

fitted=fit([fbpms fsteerers],fitted);
ParameterFit.residuals(orm,fitted,'Bpms+Steerers');

clf
ParameterFit.history.plot();

%% Combined fit
fitted=fit([fnumquad fnumskew fbpms fsteerers],fitted);
ParameterFit.residuals(orm,fitted,'all');

fitted=fit([fnumquad fnumskew fbpms fsteerers],fitted);
ParameterFit.residuals(orm,fitted,'all');

fitted=fit([fnumquad fnumskew fbpms fsteerers],fitted);
ParameterFit.residuals(orm,fitted,'all');

clf
ParameterFit.history.plot();

%% Plotting

bar(orm);               % Figure 3
bar(orm-reference);     % Figure 4

% Bpms
% H scaling
fbgh.bar(fitted.atmodel);
fbgh.bar(fitted.atmodel,'flat');
fbgh.bar(fitted.atmodel,'std');
fbgh.bar(fitted.atmodel,'mean');

% Focusing errors
% plot deltak/k
fnumquad.bar(fitted.atmodel);
fnumquad.bar(fitted.atmodel,'flat');
fnumquad.bar(fitted.atmodel,'std');
fnumquad.bar(fitted.atmodel,'mean');
%plot kn
fnumquad.bar(fitted.atmodel);
fnumquad.bar(fitted.atmodel,'absolute','flat');
fnumquad.bar(fitted.atmodel,'absolute','std');
fnumquad.bar(fitted.atmodel,'absolute','mean');



