classdef ORMNewtonFit < ParameterFit & matlab.mixin.Heterogeneous
    %ORMNewtonFit     General purpose Raphson-Newton fit
    %
    %   ORMNewtonFit defines derivatives specific for Orbit Response Matrix
    %   measurements, and provides their computation methods
    
    properties
        global_weight=1         % Comparative weight of different fits
        sum_weight=[]           % Weight of the constraint on the sum of errors
    end
    
    properties (Access=protected)
        h_derivatives           % Storage of D(h_orbit)/D(error)
        v_derivatives           % Storage of D(v_orbit)/D(error)
        dispersion_derivatives  % Storage of D(f_response)/D(error)
        tune_derivatives        % Storage of D(tunes)/D(error)
    end
    
    properties (Access=private)
        progress                % Progress function
    end
    
    methods
        function obj = ORMNewtonFit(measurement,reference,perturb_mask,perturb_fun,perturb_step,varargin)
            %ORMNEWTONFIT	General purpose Raphson-Newton fit
            %
            %FIT=ORMNEWTONFIT(ORM,REFERENCE,PERTURB_MASK,PERTURB_FUNC,PERTURB_STEP[,KEYWORD,VALUE])
            %
            % ORM:          Measured response matrix
            % REFERENCE:	Response of the reference lattice
            % PERTURB_MASK: Location of all elements where pertubations may be applied
            % PERTURB_FUNC: Function to apply a pertubation to elements marked in PERTURB_MASK
            % PERTURB_STEP: Perturbation step to apply for computing derivatives
            %
            %  keywords list
            %
            % 'WorkDir'     Directory for storage of derivatives.
            %               Default: measurement directory
            %
            % 'neigen'      Number of Eigen vectors
            %
            % 'select_errors'   Error selection
            %               Default: all
            %
            % 'h_cell'      Function computing derivatives of the response to H steerers
            % 'h_savecell'      "
            %               The keyword indicates the kind of storage used
            %               for the derivatives:
            %       cell        Cells are computed on the 1st time they are referenced
            %       savecell    Identical but the cell array is backed-up on disk
            %                   after each modification
            %               Default: null derivatives
            %
            % 'v_cell'      Function computing derivatives of the response to V steerers
            % 'v_savecell'      "
            %               Default: null derivatives
            %
            % 'f_cell'      Function computing derivatives of the response to frequency
            % 'f_savecell'      "
            %               Default: null derivatives
            %
            % 'tune_cell'	Function computing derivatives of the tunes
            % 'tune_savecell'	"
            % 'tune_saveblock'	"
            %               Default: null derivatives
            
            % All other arguments are used as (property_name, property_value)
            % pairs. This is useful when properties must be set in a well defined order.
            
            % Process arguments
            [progress,varargs]=getoption(varargin,'progress',@show_progress);            
            [h_deriv,varargs]=getarray(varargs,'h',@ORMNewtonFit.hderivs);
            [v_deriv,varargs]=getarray(varargs,'v',@ORMNewtonFit.vderivs);
            [f_deriv,varargs]=getarray(varargs,'f',@ORMNewtonFit.fderivs);
            [t_deriv,varargs]=getarray(varargs,'tune',@ORMNewtonFit.tderivs);
            
            obj=obj@ParameterFit(measurement,perturb_mask,perturb_fun,varargs{:});
            
            shlist=find(measurement.select('steerhdq'));
            svlist=find(measurement.select('steerv'));
            errlist=find(perturb_mask)';
            fn=split(func2str(perturb_fun),'.');
            perturb_name=fn{end};
            froot=fullfile(obj.working_directory,perturb_name);
            
            % Create cell arrays for the storage of derivatives
            obj.h_derivatives=h_deriv([froot '_horbit'],'LineIndex',shlist,'ColumnIndex',errlist); %#ok<FNDSB>
            obj.v_derivatives=v_deriv([froot '_vorbit'],'LineIndex',svlist,'ColumnIndex',errlist); %#ok<FNDSB>
            obj.dispersion_derivatives=f_deriv([froot '_forbit'],'ColumnIndex',errlist);
            obj.tune_derivatives=t_deriv([froot '_tunes'],'ColumnIndex',errlist);
            obj.progress=progress;
            
            function [deriv,varargs]=getarray(varargs,code,defcompute)
                % Select the storage and computation function for each derivative
                if getflag(varargs,[code '_savecell'])
                    [compute,varargs]=getoption(varargs,[code '_savecell'],[]);
                    deriv=@(fname,varargin) autocell(@compfun,varargin{:},'FileName',fname);
                else
                    [compute,varargs]=getoption(varargs,[code '_cell'],defcompute);
                    deriv=@(fname,varargin) autocell(@compfun,varargin{:});
                end
                
                function cells=compfun(is,iq)
                    % autocell-compatible computation function
                    cells=compute(reference,is,iq,perturb_fun,perturb_step,progress);
                end
            end
            
        end
        
    end
    
    methods (Sealed)
        
        function respmodel=fit(obj,respmodel,varargin)
            %FIT    Fit the model to the measurement
            %
            % NEWMODEL=FIT(OBJ,MODEL)
            %
            % OBJ:      Fit objects. If OBJ is a line vector, combine all
            %           objects for the fit. If OBJ is a column vector, fit
            %           all objects in succession
            % MODEL:    model OrbitResponse object
            
            atmodel=respmodel.atmodel;
            meas=obj(1).measurement;
            for objl=obj'
                % Solve the full system
                perturb_values=solve(objl',respmodel,varargin{:});
                % Split the perturbations
                split=arrayfun(@(ob) sum(ob.errors_set_),objl');
                increment=mat2cell(perturb_values,split,1)';
                % Apply the perturbation on each element
                for i=1:length(objl)
                    objl(i).apply_perturbations(atmodel,objl(i).global_weight.*increment{i});
                end
                respmodel=meas.get_reference(atmodel);
            end
        end
        
        function [u,s,v]=analysis(obj,varargin)
            %ANALYSIS    Analyse the SVD decomposition of the fit
            %
            % [U,S,V]=ANALYSIS(OBJ)
            % [U,S,V]=ANALYSIS(OBJ,LAYOUT)
            %   Plot the error Eigen vectors and the singular values
            %
            % OBJ:      Fit objects. If OBJ is a line vector, combine all
            %           objects for the fit. If OBJ is a column vector, fit
            %           all objects in succession
            
            t=checkarg(varargin,'check',@(a) isgraphics(a,'tiledLayout'),...
                'generate',@() newlayout(3,1,'SVD','TileSpacing','compact','Padding','compact'));
            
            % Check constraints
            cst=arrayfun(@(ob) ~isempty(ob.sum_weight),obj);
            cst_list=eye(length(obj));
            cst_list=cst_list(cst,:);
            % Build all sub-matrices
            l=cellfun(@(ob,c) ob.global_weight.*assemble(ob,c),...
                num2cell(obj,1),num2cell(cst_list,1),'UniformOutput',false);
            % Assemble sub-matrices
            lhs=cat(2,l{:});
            [u,s,v]=svd(lhs,'econ');
            lambda=diag(s);
            
            ax2=nexttile(t,3);
            semilogy(ax2,lambda);
            xlabel(ax2,'Vector #');
            xlim(ax2,[1 length(lambda)]);
            grid(ax2,'on');
            ylabel(ax2,'Singular value');
            
            ax1=nexttile(t,1,[2,1]);
            surf=pcolor(ax1,abs(v));
            colormap(ax1,'jet');
            surf.EdgeColor='none';
            ylabel(ax1,'Error #');
            title(ax1,'Singular vectors');
        end
        
    end
    
    methods (Sealed)
        
        function lhs = assemble(obj,varargin)
            %ASSEMBLE   Assemble all derivatives into a matrix
            %
            % LHS=ASSEMBLE(OBJ)
            
            weighted=~isempty(obj.sum_weight);
            cstlist=getargs(varargin,{eye(weighted)});
            meas=obj.measurement;
            errors_set = obj.errors_set_;
            bpm_set = meas.bpm_set_;
            
            % Extract derivatives from cell array to numeric array
            dh   = real(extract(obj.h_derivatives(meas.sh_set_,errors_set),bpm_set));
            dh2v = imag(extract(obj.h_derivatives(meas.sh2v_set_,errors_set),bpm_set));
            dv2h = real(extract(obj.v_derivatives(meas.sv2h_set_,errors_set),bpm_set));
            dv   = imag(extract(obj.v_derivatives(meas.sv_set_,errors_set),bpm_set));
            dd=extract(obj.dispersion_derivatives(1,errors_set),bpm_set);
            dd={real(dd) imag(dd)};
            df=cat(1,dd{meas.disp_planes});
            dd=extract(obj.tune_derivatives(1,errors_set),true);
            dd={real(dd) imag(dd)};
            dt=cat(1,dd{meas.tune_planes});
            
            % Add constraints
            constraints=zeros(length(cstlist),size(df,2));
            if weighted
                constraints(cstlist,:)=obj.sum_weight;
            end
            
            % Apply weight and combine
            lhs=[meas.orbit_weight*[dh;dh2v;dv2h;dv];...
                meas.disp_weight*df;...
                meas.tune_weight*dt;...
                constraints];
            
            function dh=extract(derivs,bpm_set)
                % Convert cell array derivs into numeric array
                nbpms=length(bpm_set);
                [~,nerrs]=size(derivs);
                hh=reshape(cat(2,derivs{:}),nbpms,[]);  % to deal with empty matrices
                dh=reshape(hh(bpm_set,:),[],nerrs);     % to select bpms
            end
        end
    end
    
    methods (Sealed, Access=protected)
        
        function apply_perturbations(obj,atmodel,perturb_values)
            %APPLY_PERTURBATIONS    Apply the computed perturbations
            %
            % APPLY_PERTURBATIONS(OBJ,ATMODEL,PERTURB_VALUES)
            %   Apply the perturbations to the give AT model
            values=num2cell(perturb_values);
            atmodel.substitute(obj.select_errors,obj.perturb_fun,values);
            
        end
        
        function perturb_values=solve(obj,respmodel,varargin)
            %SOLVE  Solve the Raphson-Newton system
            
            % Sum the number of Eigen vectors
            neigen=getoption(varargin,'neigen',sum([obj.neigen]));
            % Check constraints
            cst=arrayfun(@(ob) ~isempty(ob.sum_weight),obj);
            cst_list=eye(length(obj));
            cst_list=cst_list(cst,:);
            % Build all sub-matrices
            l=cellfun(@(ob,c) ob.global_weight.*assemble(ob,c),...
                num2cell(obj,1),num2cell(cst_list,1),'UniformOutput',false);
            % Assemble sub-matrices
            lhs=cat(2,l{:});
            % Compute right-hand side for the 1st fit object
            obj1=obj(1);
            rhs=[assemble(obj1.measurement-respmodel);...
                zeros(sum(cst),1)];
            % Solve
            perturb_values=obj1.svdsolve(lhs,rhs,neigen); %#ok<*PROPLC>
        end
        
    end
    
    methods (Static)
        
        % Functions for computing the derivatives. These functions should
        % be compiled / parallelized for efficiency
        
        
        function derivs=hderivs(~,steerers,perturbs,~,~,~)
            %HDERIVS    Compute numerically horizontal orbit derivatives
            %
            % DERIV=HDERIVS(REFMODEL,STEERERS,PERTURBS,FUNC,STEP,PROGRESS)
            %
            % REFMODEL: Theoretical response matrix
            % STEERERS: list of steerers indexes in the ring
            % PERTURBS: list of perturbation indexes in the ring
            % FUNC:     Perturbation function
            % STEP:     Perturbation step fot computing the derivative
            % PROGRESS: Progress indicator function
            
            derivs=repmat({zeros(320,1)},length(steerers),length(perturbs));
            
        end
        
        function derivs=vderivs(~,steerers,perturbs,~,~,~)
            %VDERIVS    Compute numerically vertical orbit derivatives
            %
            % DERIV=VDERIVS(REFMODEL,STEERERS,PERTURBS,FUNC,STEP,PROGRESS)
            %
            % REFMODEL: Theoretical response matrix
            % STEERERS: list of steerers indexes in the ring
            % PERTURBS: list of perturbation indexes in the ring
            % FUNC:     Perturbation function
            % STEP:     Perturbation step fot computing the derivative
            % PROGRESS: Progress indicator function
            
            derivs=repmat({zeros(320,1)},length(steerers),length(perturbs));
            
        end
        
        function derivs=fderivs(~,~,perturbs,~,~,~)
            %FDERIVS    Compute numerically frequency response derivatives
            %
            % DERIV=FDERIVS(REFMODEL,~,PERTURBS,FUNC,STEP,PROGRESS)
            %
            % REFMODEL: Theoretical response matrix
            % PERTURBS: list of perturbation indexes in the ring
            % FUNC:     Perturbation function
            % STEP:     Perturbation step fot computing the derivative
            % PROGRESS: Progress indicator function

            derivs=repmat({zeros(320,1)},1,length(perturbs));
        end
        
        function derivs=tderivs(~,~,perturbs,~,~,~)
            %TDERIVS    Compute numerically tune response derivatives
            %
            % DERIV=TDERIVS(REFMODEL,~,PERTURBS,FUNC,STEP,PROGRESS)
            %
            % REFMODEL: Theoretical response matrix
            % PERTURBS: list of perturbation indexes in the ring
            % FUNC:     Perturbation function
            % STEP:     Perturbation step fot computing the derivative
            % PROGRESS: Progress indicator function
            
            derivs=repmat({zeros(1,1)},1,length(perturbs));
        end
    end
end
