classdef SteererVIFit < ORMNewtonFit
    %STEERERVIFIT	Fit Real part of horizontal steerer errors
    
    methods
        function obj = SteererVIFit(measurement,reference,varargin)
            %STEERERVIFIT	Fit Real part of horizontal steerer errors
            %
            %STEERERVIFIT(ORM,REFERENCE[,KEYWORD,VALUE])
            %
            % ORM:          Measured response matrix
            % REFERENCE:	Response of the reference lattice
            %
            %  keywords list:
            %
            % 'WorkDir'     Directory for storage of derivatives.
            %               Default: measurement directory
            %
            % 'neigen'      Number of Eigen vectors (default: number of
            %               selected steerers
            %
            % 'select_errors'   Error selection
            %               Default: all
            %
            % All other arguments are used as (property_name, property_value)
            % pairs. This is useful when properties must be set in a well defined order.
            perturb_mask=measurement.select('steerv');
            [select_errors,varargs]=getoption(varargin,'select_errors',measurement.select_sv);
            obj=obj@ORMNewtonFit(measurement,reference,perturb_mask,@SteererVIFit.setkvimag,1.0e-4,...
                'v_cell',@SteererVIFit.vderivs,'select_errors',select_errors,varargs{:});
        end
        
        function [scale,label,tit]=get_errors(obj,atmodel,varargin)
            %GET_ERRORS     Compute V steerer scaling errors
            %
            %SCALE=GET_ERRORS(OBJ,ATMODEL[,KEYWORD,VALUE])
            %   Compute the V steerer scaling errors for the fitted ATMODEL ring
            % 
            % ATMODEL:  Test lattice
            scale=abs(atmodel.getfieldvalue(obj.perturb_mask,'AxesDef',{2},'Default',1i))-1;
            label='\Delta\theta_V/\theta_V';
            tit='Vertical steerer scaling';
        end
        
    end
    
    methods (Access=protected)
        
        function errors_set=set_errors(obj,desired_mask)
            % Set the default number of eigen vectors to the number of
            % errors (Maximum possible)
            errors_set=set_errors@ORMNewtonFit(obj,desired_mask);
            obj.neigen=sum(errors_set);
        end
        
    end
    
    methods (Access=protected, Static)

        function elem=setkvimag(elem,value)
            %SETKVIMAG	Apply imaginary vertical perturbation
            increment=[0 1i*value];
            try
                scale=elem.AxesDef;
            catch
                scale=[1 1i];
            end
            elem.AxesDef=scale + increment;
        end
        
    end
    
    methods (Static)
       
        % Functions for computing the derivatives.
        
        function derivs=vderivs(refmodel,steerers,perturbs,~,~,~)
            %VDERIVS    Compute analytically vertical orbit derivatives
            
            is=reduce_mask(refmodel,steerers,refmodel.select_sv);
            vcells=refmodel.vcells(is);
            
            derivs(1:length(steerers),1:length(perturbs))={zeros(320,1)};
            for i=1:length(steerers)
                derivs(i,perturbs==steerers(i))=vcells(i);
            end
        end
        
    end
    
end
