function ax=newaxes(varargin)
%NEWAXES	Find or create axes

odd=logical(bitget(length(varargin),1));
[name,varargs]=checkarg(varargin,'','check',@(arg) odd,'generate',@() '');
if isempty(name)
    % Create a new figure
    fig=figure();
else
    fig=findobj('Type','figure','Name',name);
    if isempty(fig)
        % Create a new figure with specified name
        fig=figure('Name',name);
    end
end
ax=axes(fig(1),varargs{:});
end
