classdef BpmHRFit < ORMNewtonFit
    %BPMHRFIT	Fit Real part of horizontal BPM errors
    
    methods
        function obj = BpmHRFit(measurement,reference,varargin)
            %BPMHRFIT	Fit Real part of horizontal BPM errors
            %
            %FIT=BPMHRFIT(ORM,REFERENCE[,KEYWORD,VALUE])
            %
            % ORM:          Measured response matrix
            % REFERENCE:	Response of the reference lattice
            %
            %  keywords list:
            %
            % 'WorkDir'     Directory for storage of derivatives.
            %               Default: measurement directory
            %
            % 'neigen'      Number of Eigen vectors (default: number of
            %               selected bpms
            %
            % 'select_errors'   Error selection
            %               Default: all
            %
            % All other arguments are used as (property_name, property_value)
            % pairs. This is useful when properties must be set in a well defined order.
            
            perturb_mask=measurement.select('bpm');
            [select_errors,varargs]=getoption(varargin,'select_serrors',perturb_mask);
            obj=obj@ORMNewtonFit(measurement,reference,perturb_mask,@BpmHRFit.setbhreal,1.0e-4,...
                'h_cell',@BpmHRFit.hderivs,'f_cell',@BpmHRFit.fderivs,...
                'select_errors',select_errors,varargs{:});
        end
        
        function [scale,label,tit]=get_errors(obj,atmodel,varargin)
            %GET_ERRORS     Compute H BPM scaling errors
            %
            %SCALE=GET_ERRORS(OBJ,ATMODEL[,KEYWORD,VALUE])
            %   Compute the H BPM scaling errors for the fitted ATMODEL ring
            % 
            % ATMODEL:  Test lattice
            scale=abs(atmodel.getfieldvalue(obj.perturb_mask,'AxesDef',{1},'Default',1))-1;
            label='\Deltax/x';
            tit='Horizontal BPM scaling';
        end
        
    end
    
    methods (Access=protected)
        
        function errors_set=set_errors(obj,desired_mask)
            % Set the default number of eigen vectors to the number of
            % errors (Maximum possible)
            errors_set=set_errors@ORMNewtonFit(obj,desired_mask);
            obj.neigen=sum(errors_set);
        end
        
    end
    
    methods (Access=protected, Static)

        function elem=setbhreal(elem,value)
            %SETBHREAL	Apply real horizontal perturbation
            increment=[value 0];
            try
                scale=elem.AxesDef;
            catch
                scale=[1 1i];
            end
            elem.AxesDef=scale + increment;
        end
        
    end
    
    methods (Static)
       
        % Functions for computing the derivatives.
        
        function derivs=hderivs(refmodel,steerers,perturbs,~,~,~)
            %HDERIVS    Compute analytically horizontal orbit derivatives
            
            iq=find(reduce_mask(refmodel,perturbs,refmodel.select('bpm')));
            is=find(reduce_mask(refmodel,steerers,refmodel.select_sh));
            [jq,js]=meshgrid(iq,is);
            derivs=arrayfun(@gener,js,jq,'UniformOutput',false);
            
            function resp=gener(js,jq)
                resp=zeros(320,1);
                resp(jq)=-refmodel.hcells{js}(jq);
            end
        end
        
        function derivs=fderivs(refmodel,~,perturbs,~,~,~)
            %FDERIVS    Compute analytically frequency response derivatives
            
            iq=find(reduce_mask(refmodel,perturbs,refmodel.select('bpm')));
            derivs=arrayfun(@gener,iq,'UniformOutput',false);
            
            function resp=gener(iq)
                resp=zeros(320,1);
                resp(iq)=-refmodel.fresp(iq);
            end
        end
        
    end
    
end
