function reference=model_response(measuredrm)
%MODEL_RESPONSE     Build model response for derivative computation
%
%REFERENCE=MODEL_RESPONSE(RMDATA)
%
% RMDATA:   structure with fields:
%               rmodel
%               Qh
%               Qv
%               hlist
%               vlist
%               delta_RF_offEnergy

% Get model
ring = atradoff(measuredrm.rmodel,'IdentityPass','auto','auto');
atmod=sr.model(ring,'reduce',true);
atmod.settune([measuredrm.Qh measuredrm.Qv]);

% Get steerer lists
hmask=extend_mask(measuredrm.hlist,atmod.select('steerhdq'));
vmask=extend_mask(measuredrm.vlist,atmod.select('steerv'));

% Get off-momentum
df=measuredrm.delta_RF_offEnergy;
circ=atmod.ll;
[~,~,~,harmnumber]=atenergy(atmod.ring);
frev=PhysConstant.speed_of_light_in_vacuum.value/circ;
ct=-circ*df/(frev+df)/harmnumber;

% Build response
reference=OrbitResponse(atmod,ct,'select_sh',hmask,'select_sv',vmask,'disp_planes',1:2,'tune_planes',1:2);
end

