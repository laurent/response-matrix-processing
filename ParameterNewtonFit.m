classdef (Abstract) ParameterNewtonFit < ParameterFit
    %PARAMETERNEWTONFIT adds to ParameterFit the information necessary to:
    %
    %- build the derivatives for a Raphson-Newton fit
    %- allow parametesr to be combined with others to perform a global fit
    
    properties
    end
    
    methods
        
    end
    
    methods (Sealed)
        
    end
    
    methods (Access=protected)
        
    end
    
    methods (Abstract)
        rhs=assemble(obj);
    end
    
end

