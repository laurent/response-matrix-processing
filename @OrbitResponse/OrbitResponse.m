classdef OrbitResponse < matlab.mixin.Copyable
    %ORBITRESPONSE Represents a measured or model orbit response matrix
    
    % Public properties: locations are expressed as logical masks or
    % list of integers, refering to the whole ring (like "refpts")
    properties (SetAccess=immutable)
        location            % data location on disk
        measurementtime     % Date and time of measurement
    end
    
    properties
        orbit_weight=1.0	% Weight of orbit fit
        disp_weight=0.1     % Weight of dispersion fit
        tune_weight=1000000	% Weight of tune fit
        disp_planes         % Actually used dispersions planes
        tune_planes         % Actually used tunes planes
    end
        
    properties (Dependent)
        % Visible properties: locations are expressed as logical masks or
        % list of integers, refering to the whole ring (like "refpts")
        select_sh           % Actually used H steerers
        select_sh2v         % Actually used H2V steerers
        select_sv2h         % Actually used V2H steerers
        select_sv           % Actually used V steerers
        bpm_ignorelist      % list of bpms to ignore
    end
    
    properties (SetAccess=protected)
        ct                  % Off-momentum expressed as orbit lengthening
        valid_bpms          % Mask of valid BPMs
        tunes               % Measured tunes
        fresp               % HV response to Delta_f/f
        %corrections        % Quad and skew corrections
    end

    properties (Dependent, SetAccess=protected)
        hresp               % H response to H steerers (array)
        h2vresp             % V response to H steerers (array)
        v2hresp             % H response to V steerers (array)
        vresp               % V response to V steerers (array)
        hcells              % HV response to H steerers (cell array)
        vcells              % HV response to V steerers (cell array)
        atmodel             % copy of the AT model (to avoid modifications)
        dpp                 % Off-momentum expressed as Delta_p/p
        available_sh        % Available H steerers
        available_sv        % Available V steerers
    end
    
    % Private properties: locations are expressed as logical masks on
    %   the subset of corresponding elements.
    properties (SetAccess=protected, GetAccess={?OrbitResponse,?ParameterFit})
        bpm_set_        	% selected BPMs             [n_bpms,1]
        sh_set_             % Subset of H steerers      [n_hst, 1]
        sh2v_set_           % Subset of H2V steerers    [n_hst, 1]
        sv2h_set_           % Subset of V2H steerers    [n_hst, 1]
        sv_set_             % Subset of V steerers      [n_vst, 1]
        atmodel_            % Protected AT model
        hdata_              % Cell array of H responses
        vdata_              % Cell array of V responses
    end
    
    methods
        
        function obj=OrbitResponse(location,varargin)
            %ORBITRESPONSE Constructor
            %
            %ORM=ORBITRESPONSE(dirname)
            %   Build a measured response matrix from a measurement directory
            %
            % DIRNAME:  Name of the measurement directory
            %
            %ORM=ORBITRESPONSE(atmodel,ct)
            %   Build - model response matrix
            %
            % ATMODEL:  AT model
            % CT:       Off-momentum expressed as orbit lengthening
            %
            %ORM=ORBITRESPONSE(...[,keyword,value])
            %  keyword is in the following list:
            %
            % 'select_sh'	Build only the response to the specified H steerers
            %               Default: all measured orbits
            %
            % 'select_sv'	Build only the response to the specified V steerers
            %               Default: all measured orbits
            
            if isa(location,'atmodel')
                atmod=location;
                location='';
            elseif ischar(location) || isstring(location)
                [atmod,measuredrm]=obj.load_file(location);
            end
            
            % Allocate storage for the responses
            obj.location=location;
            obj.atmodel_=atmod;
            obj.hdata_=cell(1,sum(atmod.select('steerhdq')));
            obj.vdata_=cell(1,sum(atmod.select('steerv')));
            obj.fresp=[];
            obj.tunes=[];
            
            if exist('measuredrm','var')
                obj.measurementtime=measuredrm.measurementtime;
                obj.FillFromMeasurement(atmod,measuredrm,varargin{:});
            else
                obj.measurementtime=datestr(now);
                obj.FillFromModel(atmod,varargin{:});
            end
            obj.disp_planes=1:2;
            obj.tune_planes=1:2;
            obj.bpm_set_=obj.valid_bpms;
        end
        
        % Implement the "-" operator
        
        function r=minus(obj,other)
            %MINUS  "-" (Minus) Substraction of response matrices
            r=obj.copy();
            r.hdata_=cellfun(@minus,obj.hdata_,other.hdata_,'UniformOutput',false,'ErrorHandler',@(s,a,b)[]);
            r.vdata_=cellfun(@minus,obj.vdata_,other.vdata_,'UniformOutput',false,'ErrorHandler',@(s,a,b)[]);
            ft=cellfun(@minus,{obj.fresp;obj.tunes},{other.fresp;other.tunes},'UniformOutput',false,'ErrorHandler',@(s,a,b)[]);
            [r.fresp,r.tunes]=deal(ft{:});
        end
        
        % Access to the model properties
        
        function varargout=select(obj,varargin)
            %SELECT Return a logical mask selecting elements of the AT model
            [varargout{1:nargout}]=obj.atmodel_.select(varargin{:});
        end
        
        function varargout=getmodelattribute(obj,varargin)
            %GETMODELATTRIBUTE  Return field values of the AT model
            [varargout{1:nargout}]=obj.atmodel_.getfieldvalue(varargin{:});
        end
        
        function value=getmodelproperty(obj,propname)
            %GETMODELPROPERTY   Return any property of the AT model
            value=obj.atmodel_.(propname);
        end
        
        function varargout=lindata(obj,varargin)
            %LINDATA	Return optics parameters of the AT model
            [varargout{1:nargout}]=obj.atmodel_.lindata(varargin{:});
        end
        
        function varargout=avedata(obj,varargin)
            %LINDATA	Return optics parameters of the AT model
            [varargout{1:nargout}]=obj.atmodel_.avedata(varargin{:});
        end
        
        function mask=reduce_mask(obj,varargin)
            mask=reduce_mask(obj.atmodel_,varargin{:});
        end
        
        function vectorrm=assemble(obj)
            %ASSEMBLE   Build the weighted objective
            orbitw=obj.orbit_weight;
            freqw=obj.disp_weight;
            tunew=obj.tune_weight;
            
            h=orbitw   * obj.hresp;
            h2v=orbitw * obj.h2vresp;
            v2h=orbitw * obj.v2hresp;
            v=orbitw   * obj.vresp;
            fhv = [real(obj.fresp) imag(obj.fresp)];
            thv = [real(obj.tunes) imag(obj.tunes)];
            f=freqw *   fhv(:,obj.disp_planes);
            tns=tunew *	thv(obj.tune_planes);
            orbits=[h h2v v2h v f];
            orbits=orbits(obj.bpm_set_,:);
            vectorrm=[orbits(:);tns(:)];
        end
        
        function nm=rnorm(varargin)
            %RNORM   Weighted evaluation function
            nm=rms(assemble(varargin{:}));
        end
        
        function res=residuals(obj)
            %RESIDUALS  Compute human-readable residuals
            res.h=residual(obj.hresp);
            res.h2v=residual(obj.h2vresp);
            res.v2h=residual(obj.v2hresp);
            res.v=residual(obj.vresp);
            res.hdisp=obj.atmodel_.alpha*residual(real(obj.fresp));
            res.vdisp=obj.atmodel_.alpha*residual(imag(obj.fresp));
            res.tune=rms(obj.tunes);
            fprintf('residual H     = %g m/rad\n', res.h);
            fprintf('residual V     = %g m/rad\n', res.v);
            fprintf('residual Hdisp = %g m\n', res.hdisp);
            fprintf('residual V2H   = %g m/rad\n', res.v2h);
            fprintf('residual H2V   = %g m/rad\n', res.h2v);
            fprintf('residual Vdisp = %g m\n', res.vdisp);
            
            function r=residual(matr)
                if isempty(matr)
                    r=NaN;
                else
                    r=rms(matr(obj.bpm_set_,:));
                end
            end
        end
        
        function orm=get_reference(obj,varargin)
            [atmod,~]=getargs(varargin,{obj.atmodel_});
            orm=OrbitResponse(atmod,obj.ct,'select_sh',obj.available_sh,...
                'select_sv',obj.available_sv,'disp_planes',1:2,'tune_planes',1:2);
        end
            
        % Property access functions
        
        function sh_mask=get.select_sh(obj)
            sh_mask=obj.atmodel.select('steerhdq');
            sh_mask(sh_mask)=obj.sh_set_;
        end
        
        function sh_mask=get.select_sh2v(obj)
            sh_mask=obj.atmodel.select('steerhdq');
            sh_mask(sh_mask)=obj.sh2v_set_;
        end
        
        function sh_mask=get.select_sv2h(obj)
            sh_mask=obj.atmodel.select('steerv');
            sh_mask(sh_mask)=obj.sv2h_set_;
        end
         
        function sv_mask=get.select_sv(obj)
            sv_mask=obj.atmodel.select('steerv');
            sv_mask(sv_mask)=obj.sv_set_;
        end
        
        function list=get.bpm_ignorelist(obj)
            list=find(~obj.bpm_set_);
        end
        
        function set.select_sh(obj,steerers)
            obj.sh_set_=reduce_mask(obj,steerers,...
                obj.atmodel.select('steerhdq'),obj.available_sh);
        end
        
        function set.select_sh2v(obj,steerers)
            obj.sh2v_set_=reduce_mask(obj,steerers,...
                obj.atmodel.select('steerhdq'),obj.available_sh);
        end
        
        function set.select_sv2h(obj,steerers)
            obj.sv2h_set_=reduce_mask(obj,steerers,...
                obj.atmodel.select('steerv'),obj.available_sv);
        end
        
        function set.select_sv(obj,steerers)
            obj.sv_set_=reduce_mask(obj,steerers,...
                obj.atmodel.select('steerv'),obj.available_sv);
        end

        function set.bpm_ignorelist(obj,ignore_list)
            nbpms=sum(obj.atmodel.select('bpm'));
            bpm_mask=true(nbpms,1);
            bpm_mask(ignore_list)=false;
            missing=bpm_mask & ~obj.valid_bpms;
            if any(missing)
                warning('ParameterFit:NotAvailable',...
                    'BPM%s not available and ignored',...
                    sprintf(' %d',find(missing)));
            end
            obj.bpm_set_=bpm_mask & obj.valid_bpms;
        end
        
        function resp=get.hresp(obj)
            resp=real(obj.extract(obj.hdata_(obj.sh_set_)));
        end
         
        function resp=get.h2vresp(obj)
            resp=imag(obj.extract(obj.hdata_(obj.sh2v_set_)));
        end
         
        function resp=get.v2hresp(obj)
            resp=real(obj.extract(obj.vdata_(obj.sv2h_set_)));
        end
         
        function resp=get.vresp(obj)
            resp=imag(obj.extract(obj.vdata_(obj.sv_set_)));
        end
         
        function resp=get.hcells(obj)
            resp=obj.hdata_(obj.sh_set_);
        end
        
        function resp=get.vcells(obj)
            resp=obj.vdata_(obj.sv_set_);
        end
               
        function value=get.atmodel(obj)
            value=obj.atmodel_.copy();
        end
        
        function value=get.dpp(obj)
            value=obj.atmodel_.dpp;
        end
        
        function steerers=get.available_sh(obj)
            steerers=obj.atmodel_.select('steerhdq');
            steerers(steerers)=~cellfun(@isempty,obj.hdata_);
        end
        
        function steerers=get.available_sv(obj)
            steerers=obj.atmodel_.select('steerv');
            steerers(steerers)=~cellfun(@isempty,obj.vdata_);
        end
        
        % In separate files:
        
        varargout=bar(obj,figure);
        varargout=plot_dispersion(obj,ihv,varargin);
    end
    
    methods (Access=protected)
        
        function cp=copyElement(obj)
            cp=copyElement@matlab.mixin.Copyable(obj);
            cp.atmodel_=copy(obj.atmodel_);
        end
        
        function FillFromMeasurement(obj,atmod,measuredrm,varargin)
            
            shmask=atmod.select('steerhdq');
            svmask=atmod.select('steerv');
            % Process keywords
            [hmask,vargs]=getoption(varargin,'select_sh',extend_mask(measuredrm.hlist,shmask));
            [vmask,vargs]=getoption(vargs,'select_sv',extend_mask(measuredrm.vlist,svmask)); %#ok<ASGLU>
            
            % get off-momentum
            df=measuredrm.delta_RF_offEnergy;
            circ=atmod.ll;
            [~,~,~,harmnumber]=atenergy(atmod.ring);
            frev=PhysConstant.speed_of_light_in_vacuum.value/circ;
            frf=frev*harmnumber;
            obj.ct=-circ*df/(frev+df)/harmnumber;
            [~,os]=findsyncorbit(atmod.ring,obj.ct);
            atmod.dpp=os(5);
            
            % Retune the model
            %tunes=stgs.tunes-fix(stgs.tunes); %#ok<*PROPLC>
            atmod.settune([measuredrm.Qh measuredrm.Qv]);
            
            % Select requested steerers
            sh_set=reduce_mask(obj,hmask,shmask);
            sv_set=reduce_mask(obj,vmask,svmask);
            takeh=sel(measuredrm.hlist,sh_set);
            takev=sel(measuredrm.vlist,sv_set);
            
            % Store responses
            obj.hdata_(sh_set)=cellfun(@(a,b) a+1i*b,...
                num2cell(measuredrm.h(:,takeh),1),...
                num2cell(measuredrm.h2v(:,takeh),1),'UniformOutput',false);
            obj.vdata_(sv_set)=cellfun(@(a,b) a+1i*b,...
                num2cell(measuredrm.v2h(:,takev),1),...
                num2cell(measuredrm.v(:,takev),1),'UniformOutput',false);
            obj.fresp=(measuredrm.rfh+1i*measuredrm.rfv).*frf;
            obj.tunes=measuredrm.Qh + 1i*measuredrm.Qv;
            
            obj.sh_set_=sh_set;
            obj.sh2v_set_=sh_set;
            obj.sv2h_set_=sv_set;
            obj.sv_set_=sv_set;
            
            % Store corrections
%             qcor=fullfile(obj.location,'quadcor.dat');
%             scor=fullfile(obj.location,'skewcor.dat');
%             obj.corrections=[SRCorrection.QuadCorrection(atmod,qcor) ...
%                 SRCorrection.SkewCorrection(atmod,scor)];
%             obj.corrections=SRCorrection.empty(1,0);
            
            
            % Check BPM validity
            meas=[measuredrm.h(:,takeh) measuredrm.h2v(:,takeh) ...
                measuredrm.v2h(:,takev),measuredrm.v(:,takev) ...
                measuredrm.rfh,measuredrm.rfv];
            obj.valid_bpms=all(isfinite(meas),2);
            
            function take=sel(matrlist,list)
                w=zeros(size(list));
                w(matrlist)=1:length(matrlist);
                take=w(list);
                if any(take==0)
                    error('OrbitResponse:missing',['missing steerers:', sprintf(' %i', find(take==0))])
                end
            end
        end
        
        
        function FillFromModel(obj,atmod,varargin)
            
            shmask=atmod.select('steerhdq');
            svmask=atmod.select('steerv');
            bpmmask=atmod.select('bpm');
            % Process arguments
            [hmask,vargs]=getoption(varargin,'select_sh',extend_mask([],shmask));
            [vmask,vargs]=getoption(vargs,'select_sv',extend_mask([],svmask));
            [disp_planes,vargs]=getoption(vargs,'disp_planes',[]); %#ok<*PROPLC>
            [tune_planes,vargs]=getoption(vargs,'tune_planes',[]);
            [progress,vargs]=getoption(vargs,'progress',@show_progress);
            [dct,corrections]=getargs(vargs,NaN,SRCorrection.empty(1,0)); %#ok<ASGLU>
            
            if isnan(dct)
                [~,o4]=findorbit4(atmod.ring,atmod.dpp);
                dct=o4(6);
            end
            obj.ct=dct;
%             obj.corrections=corrections;
%             corrections.apply(atmod);
            
            % Select requested steerers
            hmask=logical_mask(hmask,size(shmask));
            vmask=logical_mask(vmask,size(svmask));
            sh_set=reduce_mask(obj,hmask,shmask);
            sv_set=reduce_mask(obj,vmask,svmask);
            
            % Store responses
            if ~isempty(tune_planes)
                obj.tunes=obj.tune_response(atmod.ring,dct);
            end
            if ~isempty(disp_planes)
                obj.fresp=obj.f_response(atmod.ring,dct,bpmmask);
            end
            progress(sum(sh_set)+sum(sv_set),'Building model');
%             obj.hdata_(sh_set)=obj.hv_response(atmod.ring,dct,find(hmask),bpmmask,1,progress); %#ok<FNDSB>
%             obj.vdata_(sv_set)=obj.hv_response(atmod.ring,dct,find(vmask),bpmmask,2,progress); %#ok<FNDSB>
            obj.hdata_(sh_set)=obj.hv_parallel_response(atmod.ring,dct,find(hmask),bpmmask,1,progress); %#ok<FNDSB>
            obj.vdata_(sv_set)=obj.hv_parallel_response(atmod.ring,dct,find(vmask),bpmmask,2,progress); %#ok<FNDSB>
            obj.sh_set_=sh_set;
            obj.sh2v_set_=sh_set;
            obj.sv2h_set_=sv_set;
            obj.sv_set_=sv_set;
            
            % Check BPM validity (all correct)
            obj.valid_bpms=true(sum(bpmmask),1);
        end

    end
    
    methods (Static)
        
        function tunes = tune_response(ring,ct)
            %TUNE_RESPONSE	Build the model tunes
            o5=findsyncorbit(ring,ct);
            [~,tns]=atlinopt(ring,o5(5),[]);
            tunes=tns(1)+1i*tns(2);
        end
        
        function hv = f_response(ring,ct,bpmidx)
            %F_RESPONSE     Build the frequency response
            
                % compute BPM transformation only once
                [v11,v12,v21,v22]=OrbitResponse.bpm_transform(ring,bpmidx);
                
                dff=5.0E-7;
                circ=findspos(ring,length(ring)+1);
                d2ct=-circ*dff;
                orbitf=(findsyncorbit(ring,ct+d2ct/2,bpmidx)-...
                    findsyncorbit(ring,ct-d2ct/2,bpmidx))/dff;
                h=orbitf(1,:)';
                v=orbitf(3,:)';
                % Apply BPM errors
                hv=(v11.*h+v12.*v) + 1i*(v21.*h+v22.*v);
                %hv=h+1i*v;
        end
        
        function HV=hv_response(ring,ct,steerers,bpmidx,ihv,progress)
            %HV_RESPONSE    Build the response to a set of steerers
            %
            %HV_RESPONSE(RING,CT,STEERIDX,BPMIDX,IHV)
            
            % compute BPM transformation only once
            [v11,v12,v21,v22]=OrbitResponse.bpm_transform(ring,bpmidx);
            HV=arrayfun(@orbitcor,steerers,'UniformOutput',false);
            
            function hv=orbitcor(sidx)
                [h,v]=OrbitResponse.steerer_response(ring,ct,sidx,bpmidx,ihv);
                % Apply BPM errors
                hv=(v11.*h+v12.*v) + 1i*(v21.*h+v22.*v);
                %hv=h+1i*v;
                progress();
            end
            
        end
        
        function HV=hv_parallel_response(ring,ct,steerers,bpmidx,ihv,progress)
            %HV_RESPONSE    Build the response to a set of steerers
            %
            %HV_RESPONSE(RING,CT,STEERIDX,BPMIDX,IHV)
            
            % compute BPM transformation only once
            [v11,v12,v21,v22]=OrbitResponse.bpm_transform(ring,bpmidx);
            ns=length(steerers);
            HV=cell(1,ns);
            parfor i=1:ns
                [h,v]=OrbitResponse.steerer_response(ring,ct,steerers(i),bpmidx,ihv);
                % Apply BPM errors
                HV{i}=(v11.*h+v12.*v) + 1i*(v21.*h+v22.*v);
            end
            progress(ns);
        end
        
        function [H,V]=steerer_response(ring,ct,sidx,bpmidx,ihv)
            %STEERER_RESPONSE   Build the response to a single steerer
            %
            %STEERER_RESPONSE(RING,CT,STEERIDX,BPMIDX,IHV,CORRECTFLAG)
            
            delta=0.000025;
            deltahv=[0 0];
            deltahv(ihv)=0.5*delta;
            % Apply steerer scale and rotation
            deltahv=kick_transform(ring,sidx,deltahv);
            khv0 = cell2mat(atgetfieldvalues(ring,sidx,'KickAngle'));
            
            [hp,vp]=measfun(ring,ct,sidx,bpmidx,khv0+deltahv);
            [hm,vm]=measfun(ring,ct,sidx,bpmidx,khv0-deltahv);
            
            H=(hp-hm)./delta;
            V=(vp-vm)./delta;
            
            function [h,v]=measfun(ring,ct,sidx,bpmidx,KHV)
                % set steerer
                rs = atsetfieldvalues(ring,sidx,'KickAngle',KHV);
                % get orbit
                tr = findsyncorbit(rs,ct,bpmidx);
                % return it
                h=tr(1,:)';
                v=tr(3,:)';
            end
            
            function khv2 = kick_transform(ring,sidx,khv)
                % The tranformation matrix is: [real(scale);imag(scale)]
                try
                    kscale=ring{sidx}.AxesDef;
                catch
                    kscale=[1 1i];
                end
                khv2=khv*[real(kscale);imag(kscale)]';
            end
            
        end
        
        function [v11,v12,v21,v22]=bpm_transform(ring,bpmidx)
            % The tranformation matrix is: [real(scale);imag(scale)]
            % It is inverted explictly (faster than "inv")
            bscale=cell2mat(atgetfieldvalues(ring,bpmidx,'AxesDef','Default',[1 1i]));
            rr=real(bscale);
            ii=imag(bscale);
            det=rr(:,1).*ii(:,2)-ii(:,1).*rr(:,2);
            v11=ii(:,2)./det;
            v21=-ii(:,1)./det;
            v12=-rr(:,2)./det;
            v22=rr(:,1)./det;
        end
        
        [ex,ey]=readpinholes(workingdirectory,measurementtime,doplot);
        correctdrift = driftcontrol(measobj);
        response = measure_one_actuator(measobj,mode,variation,actuatorindex);
        run_response_matrix_measurement(machine,varargin);
        plotrm(measobj);
    end
    
    methods (Static, Access=protected)
        
        function [atmod,measuredrm]=load_file(dname,atmod)
            %LOAD_FILE	Load measurement files
            measuredrm = load(fullfile(dname,'OrbitResponseMatrix.mat'));
            if ~all(isfield(measuredrm,{'Qh','Qv','delta_RF_offEnergy'}))
                stgs=load(fullfile(dname,'settings.mat'));
                measuredrm.Qh=stgs.tunes(1);
                measuredrm.Qv=stgs.tunes(2);
                measuredrm.delta_RF_offEnergy=stgs.deltafrf;
            end
            if nargin < 2
                try
                    ring = atradoff(measuredrm.rmodel,'IdentityPass','auto','auto');
                catch
                    a=load(fullfile(dname,'optics','betamodel.mat'),'betamodel');
                    ring=atradoff(a.betamodel,'IdentityPass','auto','auto');
                end
                atmod=sr.model(ring,'reduce',true);
            end
        end
        
        function hvresp=extract(orbits)
            hvresp=cat(2,orbits{:});    % Faster
            % hvresp=cat(2,orbits{:});  % Slower
        end
end
end

function v=rms(matr)
vect=matr(:);
v=sqrt(sum(vect.*vect)/length(vect));
end

