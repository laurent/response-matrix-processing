function varargout=bar(obj,varargin)
%BAR	Plot orbit standard deviations
%
%BAR(OBJ)           Plot H and V std orbits in a new figure
%BAR(OBJ,LAYOUT)    Plot H and V std orbits in a the specified layout
%
%LAYOUT=BAR(...)    Returns the layout object. The 4 axes can be obtained
%                   by "ax1=nexttile(layout,1);" etc.

t=checkarg(varargin,'check',@(a) isgraphics(a,'tiledLayout'),...
    'generate',@() newlayout(2,2,'TileSpacing','compact','Padding','compact'));
labl='response';
hresp=obj.hresp;        % Single computation
hresp(~obj.bpm_set_,:)=NaN;
[nbpm,nsteer]=size(hresp);

h1=nexttile(t,1);
bar(h1,std(hresp,1,1,'omitnan'));
set(h1,'Xlim',[0 nsteer+1]);
title(h1,['H rms ' labl]);
ylabel('std h c.o.');
xlabel('h corrector');

h3=nexttile(t,3);
bar(h3,std(hresp,1,2,'omitnan'));
set(h3,'Xlim',[0 nbpm+1]);
ylabel('std h c.o.');
xlabel('h bpm');

vresp=obj.vresp;        % Single computation
vresp(~obj.bpm_set_,:)=NaN;
[nbpm,nsteer]=size(vresp);

h2=nexttile(t,2);
bar(h2,std(vresp,1,1,'omitnan'));
set(h2,'Xlim',[0 nsteer+1]);
title(h2,['V rms ' labl]);
ylabel('std v c.o.');
xlabel('v corrector');

h4=nexttile(t,4);
bar(h4,std(vresp,1,2,'omitnan'));
set(h4,'Xlim',[0 nbpm+1]);
ylabel('std v c.o.');
xlabel('v bpm');

if nargout > 0
    varargout={t};
end
end