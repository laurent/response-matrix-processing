function response = measure_one_actuator(obj,mode,variation,actuatorindex)
% method for generic orbit measurement
%
% 'mode' may be:
% 'H' : horizontal response
% 'V' : vertical response
% 'F' : frequency response
%
% actuatorindex : steerer index. for mode F it is not used.
%
% returns NOT normalized response.

if length(actuatorindex)>1
    error('too many actuators')
end

disab = false;

% get individual device attribute
switch mode
    case 'H'
        A = obj.RingControl.sh;% moovable class object instance
        
        filename = ['steer' mode num2str(obj.hlist(actuatorindex),'%.3d')];
        hvlist=obj.hlist;
        
    case 'V'
        A = obj.RingControl.sv;% moovable class object instance
        
        filename = ['steer' mode num2str(obj.vlist(actuatorindex),'%.3d')];
        hvlist=obj.vlist;
        
        
    case 'F'
        A = obj.RingControl.rf; % moovable class object instance
        
        filename = ['steer' 'F00' ];
        hvlist=1;
        actuatorindex = 1; % only one RF frequency.
        
end


% check if file already exist, in case, skip.
if exist(fullfile(obj.workingdirectory,[filename '.mat']),'file')
    disp(['File ' filename ' already exists. Skip measurement']);
    a = load(fullfile(obj.workingdirectory,[filename,'.mat']));
    response=a.response;
    try
        real_variation=a.real_variation;
    catch
        real_variation = variation;
    end
    %  filenametotxt;
    return
end

% check if it is enabled
switch mode
    case 'H'
        
        en = obj.RingControl.getEnabledHsteerers;
        if en(obj.hlist(actuatorindex))
            disp(['Measuring ' obj.hnames{actuatorindex} ...
                ' ' num2str(actuatorindex) ' / ' num2str(length(obj.hnames))])
        else % if not enabled skip measurement
            warning([obj.hnames{actuatorindex} ' is not enabled '...
                '(either in orbit correction or itself). SKIPPED'])
            response = zeros(size(obj.o0));
            real_variation = 1;
            disab = true;
        end
        
    case 'V'
        
        en = obj.RingControl.getEnabledVsteerers;
        if en(obj.vlist(actuatorindex))
            disp(['Measuring ' obj.vnames{actuatorindex} ...
                ' ' num2str(actuatorindex) ' / ' num2str(length(obj.vnames))])
        else % if not enabled skip measurement
            warning([obj.vnames{actuatorindex} ' is not enabled '...
                '(either in orbit correction or itself). SKIPPED'])
            response = NaN(size(obj.o0));
            real_variation = 1;
            disab = true;
        end
        
    case 'F'
        
        disp('RF response')
        
end

disp('NEW reference initial orbit')
% pause(additional_wait_time)
obj.o0 = obj.measfun();  % take new reference orbit measurement.

if ~disab
    % initial value of actuator
    K0 = A.get;
    DKp = zeros(size(K0)); % initialize array of real variation
    DKm = zeros(size(K0));
    
    if obj.twoside % double sided measurement
        % up
        Kp=K0;
        Kp(hvlist(actuatorindex)) = K0(hvlist(actuatorindex)) + 0.5*variation;
        disp(['UP: ' num2str(K0(hvlist(actuatorindex)),'%3.2e') ...
            ' + ' num2str(0.5*variation,'%3.2e') ...
            ' = ' num2str(Kp(hvlist(actuatorindex)),'%3.2e')]);
        switch mode
            case 'H'
                dev = tango.Device(['srmag/' obj.hnames{actuatorindex}]);
            case 'V'
                dev = tango.Device(['srmag/' obj.vnames{actuatorindex}]);
            case 'F'
                A.set(Kp); % moovable set also waits for setpoint reach.
        end
        dev.Strength = Kp(hvlist(actuatorindex));
        
        % get real variation in case it could not reach set point
        pause(obj.additional_wait_time)
        DKp = A.get - K0;
        disp(['Asked variation of: ' num2str(0.5*variation) ...
            ' got ' num2str(DKp(hvlist(actuatorindex)))]);
        
        op = obj.measfun();
        disp(['Dorbit: ' num2str(std2(op'-obj.o0'))]);
        disp(['+ orbit rms: ' num2str(std2(op([1,2],:)'))]);
        
        % down
        Km=K0;
        Km(hvlist(actuatorindex)) = K0(hvlist(actuatorindex)) - 0.5*variation;
        disp(['DOWN: ' num2str(K0(hvlist(actuatorindex)),'%3.2e') ...
            ' - ' num2str(0.5*variation,'%3.2e') ...
            ' = ' num2str(Km(hvlist(actuatorindex)),'%3.2e')]);
        %A.set(Km);% moovable set also waits for setpoint reach.
        switch mode
            case {'H','V'}
                dev.Strength = Km(hvlist(actuatorindex));
            case 'F'
                A.set(Km); % moovable set also waits for setpoint reach.
        end
        
        % get real variation in case it could not reach set point
        pause(obj.additional_wait_time)
        DKm = A.get - K0;
        disp(['Asked variation of: ' num2str(-0.5*variation) ...
            ' got ' num2str(DKm(hvlist(actuatorindex)))]);
        
        om = obj.measfun();
        disp(['Dorbit: ' num2str(std2(om'-obj.o0'))]);
        disp(['- orbit rms: ' num2str(std2(om([1,2],:)'))]);
        
    else % single sided
        error('not up to date')
        Kp=K0;
        Kp(hvlist(actuatorindex)) = K0(hvlist(actuatorindex)) + variation;
        disp(['UP: ' num2str(K0(hvlist(actuatorindex)),'%3.2e') ...
            ' + ' num2str(variation,'%3.2e') ...
            ' = ' num2str(Kp(hvlist(actuatorindex)),'%3.2e')]);
        A.set(Kp);% moovable set also waits for setpoint reach.
        
        % get rea variation
        pause(obj.additional_wait_time)
        DKp = A.get - K0;
        disp(['Asked variation of: ' num2str(0.5*variation) ...
            ' got ' num2str(DKp(hvlist(actuatorindex)))]);
        
        op = obj.measfun();
        disp(['Dorbit: ' num2str(std2(op'-obj.o0'))]);
        disp(['+ orbit rms: ' num2str(std2(op([1,2],:)'))]);
        
        om = obj.o0;
        DKm = K0 - K0;
        
    end
    
    % return to intial value
    
    switch mode
        case {'H','V'}
            dev.Strength = K0(hvlist(actuatorindex));
        case 'F'
            A.set(K0); % moovable set also waits for setpoint reach.
    end
    % A.set(K0);
    pause(obj.additional_wait_time)
    
    % compute response (not normalized)
    response = (op-om)  ;
    
    real_variation = DKp(hvlist(actuatorindex)) - DKm(hvlist(actuatorindex));
    
    if strcmp(getenv('TANGO_HOST'),'ebs-simu:10000')
        
        warning('introducing shift to simulate hysteresis')
        KII=K0;
        
        KII(hvlist(actuatorindex)) = K0(hvlist(actuatorindex)) + variation*0.01;
        
        A.set(KII);
        pause(obj.additional_wait_time)
        
    end
    
    % check that orbit is back to its place.
    correctdrift = obj.driftcontrol; % this function returns an ERROR if orbit changed befor and after measurement
    
    % correctdrift = false;
    
    if correctdrift
        %                 prompt = 'Do you want to correct the orbit drift? Y/N [Y]: ';
        %                 str = input(prompt,'s');
        %                 if isempty(str)
        %                     str = 'Y';
        %                 end
        str= 'Y';
        if strcmp(str,'Y')
            if not(strcmp(mode,'F'))
                odrift = obj.measfun();  % take new reference orbit measurement.
                ic = 1;
                while any(std(odrift([1,2],:)-obj.o0([1,2],:),1,2)>obj.hreshold_orbit_drifting) && ic<3
                    
                    switch mode
                        case 'H'
                            disp(['correct drift before next measurement: ' num2str(std(odrift(1,:)-obj.o0(1,:)))])
                            R = obj.HRM0(:,hvlist(actuatorindex));
                            % R = (response(1,:)./real_variation)';
                            cordrift =  R \(odrift(1,:)-obj.o0(1,:))';
                        case 'V'
                            disp(['correct drift before next measurement: ' num2str(std(odrift(2,:)-obj.o0(2,:)))])
                            R = obj.VRM0(:,hvlist(actuatorindex));
                            % R = (response(2,:)./real_variation)';
                            cordrift =  R \(odrift(2,:)-obj.o0(2,:))';
                            % cordrift =  (response(2,:)./real_variation)'\(odrift(2,:)-o0(2,:))';
                    end
                    
                    Kp=K0;
                    Kp(hvlist(actuatorindex)) = K0(hvlist(actuatorindex)) - cordrift ;
                    %A.set(Kp);
                    %dev = tango.Device(['srmag/' hnames{actuatorindex}]);
                    
                    dev.Strength = Kp(hvlist(actuatorindex));
                    
                    pause(obj.additional_wait_time)
                    ic = ic + 1;
                    odrift_aft = obj.measfun();  % take new reference orbit measurement.
                    
                    %                         figure;
                    %                         plot([odrift_aft(1,:)-o0(1,:);odrift(1,:)-o0(1,:)]');
                    %                         legend('after','before');
                    %                         std(odrift_aft(1,:)-o0(1,:))
                    %                         std(odrift(1,:)-o0(1,:))
                    %                         figure;
                    %                         plot([odrift_aft(2,:)-o0(2,:);odrift(2,:)-o0(2,:)]');
                    %                         legend('after','before');
                    
                    std(odrift_aft(2,:)-obj.o0(2,:))
                    std(odrift(2,:)-obj.o0(2,:))
                    
                    odrift = odrift_aft;
                end
            end
        else
            disp('not correcting orbit drift')
        end
    end
    
    
end

% get pinholes sizes variations
[ex,ey]=obj.readpinholes(false);

% save file also if disabled during measurement (all zeros are stored)
%matlab
save(fullfile(obj.workingdirectory,filename),'response','variation','real_variation','mode','ex','ey');
% text
filenametotxt;

    function filenametotxt
        %text (same file names and format of respmat.py
        switch mode
            case 'H'
                fh = fopen([filename ''],'w');
                fv = fopen([strrep(filename,'H','H2V') ''],'w');
                devname = obj.hnames{actuatorindex};
            case 'V'
                fh = fopen([strrep(filename,'V','V2H') ''],'w');
                fv = fopen([filename ''],'w');
                devname = obj.vnames{actuatorindex};
            case 'F'
                fh = fopen([strrep(filename,'F','F2H') ''],'w');
                fv = fopen([strrep(filename,'F','F2V') ''],'w');
                devname = A.attr_name;
        end
        
        
        fprintf(fh,['%.12f\t%s,%.1d\t' repmat('%.12f\t',1,obj.NBPM-1) '%.12f'],...
            variation,devname,obj.algo,response(1,:));
        fclose(fh);
        fprintf(fv,['%.12f\t%s,%.1d\t' repmat('%.12f\t',1,obj.NBPM-1) '%.12f'],...
            variation,devname,obj.algo,response(2,:));
        fclose(fv);
    end

end