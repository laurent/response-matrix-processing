function run_response_matrix_measurement(machine,varargin)
% run RF, Hor, Ver response matrix measurement
% inputs:
% machine (string): 'sr','ebs-simu','none'
%
% optional inputs:    ...,'name',default,... : explaination
%
% ...,'Hsteerers',1:20:end,... : index of steerers among all H
% steerers to use for horizontal response matrix measurement
%
% ...,'Vsteerers',1:20:end,... : index of steerers among all V
% steerers to use for vertical response matrix measurement
%
% ...,'Frequency',true,... : do/skip frequency response
%
% ...,'TwoSide',true,... : two or one side measurement
%
% ...,'delta',[10e-5 10e-5 200],... : full variation H, V, RF
%
% ...,'ignoreinch',true,... : unused
%
% ...,'num_acquisitions',5,... : closed orbit acquisitions
%
% ...,'threshold_orbit_drifting',1e-5,... : if orbit difference
% after steerers setting is larger than this value a correction
% is triggered using the steerers used for RM
%
% ...,'algorithm',0,... : unused
%
% ...,'workingdirectory','',... : directory where the data
% will be stored
%
% ...,'additional_wait_time',5,... : time to wait between
% two magnet settings
%
% ...,'at_lattice',{},... : an AT lattice reference for
% measurement
%
% ...,'load_measurement_folder','',... : load data from given folder
% ...,'load_measurement_file','',... : load data from given file
%
%
%see also: RingControl
global APPHOME

obj=struct();
obj.machine = machine;

% indexes and lattice default from ATmodel
atmod = sr.model('reduce',true);
obj.rmodel = atmod.ring;
obj.indBPM = atmod.get(0,'bpm');
obj.indHst = sort([atmod.get(0,'steerh'); atmod.get(0,'dq')]);%atmod.get(0,'steerh');
obj.indVst = atmod.get(0,'steerv');
obj.h0 = NaN(size(obj.indHst));
obj.v0 = NaN(size(obj.indVst));

try
    obj.HRM0=load('/operation/beamdyn/matlab/optics/sr/theory/h_resp.csv');
    obj.VRM0=load('/operation/beamdyn/matlab/optics/sr/theory/v_resp.csv');
catch
    obj.HRM0=load([APPHOME '/optics/sr/theory/h_resp.csv']);
    obj.VRM0=load([APPHOME '/optics/sr/theory/v_resp.csv']);
end

obj.measurementtime = datestr(now);

%initial settings:

obj.NHall = length(obj.h0);
obj.NVall = length(obj.v0);

%% parse inputs

p = inputParser;

addParameter(p,'Hsteerers',1:(obj.NHall/32*8):obj.NHall,@(x)(isnumeric(x) | islogical(x)));
addParameter(p,'Vsteerers',1:(obj.NVall/32*8):obj.NVall,@(x)(isnumeric(x) | islogical(x)));
addParameter(p,'Frequency',true,@islogical);
addParameter(p,'TwoSide',true,@islogical);
addParameter(p,'delta',[10e-5 10e-5 200],@(x)(isnumeric(x) & length(x)==3));
addParameter(p,'ignoreinch',false);
addParameter(p,'num_acquisitions',5);
addParameter(p,'threshold_orbit_drifting',1e-5);
addParameter(p,'algorithm',0);
addParameter(p,'workingdirectory',pwd);
addParameter(p,'additional_wait_time',5);
addParameter(p,'at_lattice',{},@iscell);
addParameter(p,'load_measurement_folder','',@ischar);
addParameter(p,'load_measurement_file','',@ischar);

parse(p,varargin{:});


obj.RF          = p.Results.Frequency;
obj.twoside     = p.Results.TwoSide;
delta       = p.Results.delta;
obj.deltah  = delta(1);
obj.deltav  = delta(2);
obj.deltarf = delta(3);
obj.ignoreinch  = p.Results.ignoreinch;
obj.nacq        = p.Results.num_acquisitions;
obj.threshold_orbit_drifting = p.Results.threshold_orbit_drifting;
obj.algo        = p.Results.algorithm;

obj.workingdirectory = p.Results.workingdirectory;
obj.additional_wait_time = p.Results.additional_wait_time;

% if a lattice is proveded, update all indexes
newlat = p.Results.at_lattice;
if ~isempty(newlat)
    disp('user defined lattice. computing indexes')
    
    atmod = sr.model(newlat,'reduce',true);
    obj.rmodel = atmod.ring;
    obj.indBPM = atmod.get(0,'bpm');
    obj.indHst = sort([atmod.get(0,'steerh'); atmod.get(0,'dq')]);
    obj.indVst = atmod.get(0,'steerv');
    
end

obj.hlist       = p.Results.Hsteerers;
obj.vlist       = p.Results.Vsteerers;

% make sure h and vlist are arrays
if islogical(obj.hlist)
    obj.hlist=find(obj.hlist);
end
if islogical(obj.vlist)
    obj.vlist=find(obj.vlist);
end

switch obj.machine
    case 'ebs-simu'
        obj.hnames = sr.hsteername(obj.hlist','hst-');
        obj.vnames = sr.vsteername(obj.vlist');
        obj.additional_wait_time = 10;
        
    case 'sr'
        obj.hnames = sr.hsteername(obj.hlist','hst-');
        obj.vnames = sr.vsteername(obj.vlist');
        
        %
        %     case 'esrf-sy'
        %
        %     case 'esrf-tl2'
    case 'none'
        disp(' ----> No interface to control system <----- ')
        obj.hnames = arrayfun(@(a)['hsteerer_' num2str(a,'%.4d')],obj.hlist,'un',0);
        obj.vnames = arrayfun(@(a)['vsteerer_' num2str(a,'%.4d')],obj.hlist,'un',0);
        
    otherwise
        warning(['Orbit Reponse Matrix not defined for ' obj.machine]);
        obj.hnames = arrayfun(@(a)['hsteerer_' num2str(a,'%.4d')],obj.hlist,'un',0);
        obj.vnames = arrayfun(@(a)['vsteerer_' num2str(a,'%.4d')],obj.hlist,'un',0);
end

obj.NH = length(obj.hlist);
obj.NV = length(obj.vlist);

% initialize orbit to empty
obj.o0 = zeros(3,length(obj.indBPM));

obj.NBPM = length(obj.o0(1,:));
pause(obj.additional_wait_time)

% initialize measured response matrices to NaN
obj.h = NaN(obj.NBPM,obj.NH);
obj.v = NaN(obj.NBPM,obj.NV);

obj.rfh = NaN(obj.NBPM,1);
obj.rfv = NaN(obj.NBPM,1);

obj.h2v = NaN(obj.NBPM,obj.NH); % horizontal steerer to vertical orbit
obj.v2h = NaN(obj.NBPM,obj.NV); % vertical steerer to horizontal orbit

% initialize simulated matrices, will be overwritten by call
% to simulate_response
obj.hS = obj.h;
obj.vS = obj.v;

obj.rfhS = obj.rfh;
obj.rfvS = obj.rfv;

obj.h2vS = obj.h2v; % horizontal steerer to vertical orbit
obj.v2hS = obj.v2h; % vertical steerer to horizontal orbit

obj.verbose = false;

% init fomr measurement folder
if ~isempty(p.Results.load_measurement_folder)
    obj.InitFromMeasurement(...
        p.Results.load_measurement_folder);
end

if ~isempty(p.Results.load_measurement_file)
    obj.InitFromMeasurement(...
        '','file',p.Results.load_measurement_file);
end

if obj.verbose
    OrbitResponse.plotrm(obj);
end
%%

RC = RingControl(obj.machine);

% initial parameters and RingControl instance
obj.Qh = RC.h_tune();
obj.Qv = RC.v_tune();
obj.h0 = RC.sh.get;
obj.v0 = RC.sv.get;
obj.rf0 = RC.rf.get;

% CHECK AUTOCOR IS NOT ON.
%autocor=tango.Device(obj.autocor);
if isa(RC.autocor,'function_handle')
    state_autocor = RC.autocor();
    if  ~strcmp(state_autocor,'Off')
        error('Autcor is not OFF');
    end
end

if ~strcmp(obj.machine,'none')
    
    obj.readpinholes(true);
    
end


% initial orbit
RC.n_acquisitions = obj.nacq; % Overwrite RingControl property

% orbit measurement. returns [bpm hor.;bpm ver.].
obj.measfun=@()RC.measureorbit('ignoreinch',obj.ignoreinch,'algorithm',obj.algo);

if ~strcmp(obj.machine,'none')
    obj.o0 = obj.measfun();  % reference orbit measurement.
end

obj.RingControl = RC; % pass the ring control class instance to other methods.

initial_waittime = obj.additional_wait_time;
obj.additional_wait_time=20;

if obj.RF
    disp(['Measure Orbit RM for RF frequency. Initial f_RF: ' num2str(obj.rf0,'%9.2f') ' Hz']);
    try
        % measure
        resp = OrbitResponse.measure_one_actuator(obj,'F',obj.deltarf,1);
        
        % assign in global RM
        obj.rfh(:,1) = resp(1,:)'./obj.deltarf;
        obj.rfv(:,1) = resp(2,:)'./obj.deltarf;
        
        % plot
        OrbitResponse.plotrm(obj);
        
    catch err
        disp(err);
        warning('Error evaluating Frequency response');
        str = input('contiune?','s');
        if ~isempty(str)
            return
        end
        
    end
end

% figure for response matrix evolution display.
clear resp;

obj.additional_wait_time=initial_waittime;

% horizontal response matrix
disp(['Measure Orbit RM for ' num2str(length(obj.hlist)) ' / ' num2str(obj.NHall) ' horizontal correctors']);

for ii = 1:length(obj.hlist)
    
    if ii == 1
        tic;
    end
    
    try
        % measure
        resp = OrbitResponse.measure_one_actuator(obj,'H',obj.deltah,ii);
        
        % assign in global RM
        obj.h(:,ii) = resp(1,:)'./obj.deltah;
        obj.h2v(:,ii) = resp(2,:)'./obj.deltah;
        
        % plot
        OrbitResponse.plotrm(obj);
        
    catch err
        disp(err);
        error(['Error evaluating response for ' obj.hnames{ii}]);
    end
    
    if ii == 1
        te = toc;
        disp(['time for 1 h steerer: ' num2str((te)/60) ' min']) ;
    else
        ff = gcf;
        ff.Name = (['O.R.M. wait: ' num2str((length(obj.hlist)+length(obj.vlist)+1-ii-1)*(te)/60) ' min']) ;
    end
    
end

disp(['Measure Orbit RM for ' num2str(length(obj.vlist)) ' / ' num2str(obj.NVall) ' vertical correctors']);
for ii = 1:length(obj.vlist)
    
    if ii == 1
        tic;
    end
    
    
    try
        % measure
        resp = OrbitResponse.measure_one_actuator(obj,'V',obj.deltav,ii);
        
        % assign in global RM
        obj.v2h(:,ii) = resp(1,:)'./obj.deltav;
        obj.v(:,ii) = resp(2,:)'./obj.deltav;
        
        % plot
        OrbitResponse.plotrm(obj);
        
        
    catch err
        disp(err);
        error(['Error evaluating response for ' obj.vnames{ii}]);
    end
    
    if ii == 1
        te = toc;
        disp(['time for 1 v steerer: ' num2str((te)/60) ' min']) ;
    else
        ff = gcf;
        ff.Name = (['O.R.M. wait: ' num2str((length(obj.vlist)-ii)*(te)/60) ' min']) ;
    end
    
    
end

measurementtime = obj.measurementtime;
Qh = obj.Qh;
Qv = obj.Qv;
h = obj.h;
v = obj.v;
rfh = obj.rfh;
rfv = obj.rfv;
h2v = obj.h2v;
v2h = obj.v2h;
hlist = obj.hlist;
vlist = obj.vlist;
rmodel = obj.rmodel;
machine = obj.machine;

save(fullfile(obj.workingdirectory,'OrbitResponseMatrix.mat'),...
    'h','v','h2v','v2h','rfh','rfv','measurementtime',...
    'hlist','vlist','Qh','Qv','rmodel','machine');

% save whole object.

% save all properties of object.
warning('off');
d = obj;
d.rmfigure =[];
d.axH = [];
d.axV = [];
warning('on');
save(fullfile(obj.workingdirectory,'ORM.mat'),'-struct','d');
disp('End of measurement');


%             % csv files for operation
%             switch obj.machine
%                 case 'ebs-simu'
%                     if obj.NH == obj.NHall && obj.NV == obj.NVall
%                         sr.autocor_model(obj.rmodel,'exp',pwd);
%                     else
%                         warning('CSV file not created, partial measurement only.')
%                     end
%                 otherwise
%             end

% % restore ringcontrol object to initial state.
% obj.n_acquisitions = na0;


end