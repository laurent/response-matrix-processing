function plotrm(obj)
% plot RM acquisition progress

% figure for response matrix evolution display.
if isempty(obj.rmfigure)
    obj.rmfigure = figure('name','orbit response matrix','units','normalized','Position',[0.05,0.2,0.9,0.6]);
    subplot(2,1,1);
    obj.axH = gca;
    subplot(2,1,2);
    obj.axV = gca;
else
    try
        figure(obj.rmfigure)
    catch
        disp('figure has been closed, new one')
        obj.rmfigure = figure('name','orbit response matrix','units','normalized','Position',[0.05,0.2,0.9,0.6]);
        subplot(2,1,1);
        obj.axH = gca;
        subplot(2,1,2);
        obj.axV = gca;
    end
end

% clar existing data
cla(obj.axH);
cla(obj.axV);

% horizontal
stdh = std2([obj.h, obj.rfh]);
stdv = std2([obj.h2v,obj.rfv]);
stdvalues =[ stdh ; stdv]';
bH=bar(obj.axH,stdvalues*1e3);
set(obj.rmfigure,'CurrentAxes',obj.axH);
hold on;
stdh0 = [std2([obj.hS(:,obj.hlist)]),std2(obj.rfhS)];
stdv0 = [std2([obj.h2vS(:,obj.hlist)]),std2(obj.rfvS)];
stdvalues =[ stdh0 ; stdv0]';
bar(obj.axH,stdvalues*1e3,'FaceColor','none','EdgeColor',[1 0 0]);

obj.axH.XLim = [0, obj.NH+2];
obj.axH.XTick = 1:length(obj.hlist)+1;
obj.axH.XTickLabel=[obj.hnames;'frequency';];%obj.rf.attr_name];
obj.axH.XTickLabelRotation = 90;
obj.axH.Title.String= 'horizontal steerers response';
obj.axH.YLabel.String = 'std of orbit response [mm]';
legend('hor.','ver.','expected');
%             hold on;
%             bH(1).YDataSource ='stdh';
%             bH(2).YDataSource ='stdv';
%
% vertical
stdh = std2(obj.v2h);
stdv = std2(obj.v);
stdvalues =[ stdh ; stdv]';
bv=bar(obj.axV,stdvalues*1e3);
set(obj.rmfigure,'CurrentAxes',obj.axV);
hold on;
stdv0 = [std2([obj.vS(:,obj.vlist)])];
stdh0 = [std2([obj.v2hS(:,obj.vlist)])];
stdvalues =[ stdh0 ; stdv0]';
bar(obj.axV,stdvalues*1e3,'FaceColor','none','EdgeColor',[0 0 1]);

obj.axV.XLim = [0, obj.NV+1];
obj.axV.XTick = 1:length(obj.vlist);
obj.axV.XTickLabel=obj.vnames;
obj.axV.XTickLabelRotation = 90;
obj.axV.Title.String= 'vertical steerers response';
obj.axV.YLabel.String = 'std of orbit response [mm]';
legend('hor.','ver.','expected');
%             hold on;
%             bv(1).YDataSource ='stdh';
%             bv(2).YDataSource ='stdv';

drawnow;
end