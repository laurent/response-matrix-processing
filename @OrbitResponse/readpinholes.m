function [ex,ey]=readpinholes(workingdirectory,measurementtime,doplot)
% function to loop trough cameras measurement. will be replaced by
% srdiag/beam-emittance/all/Emittance_H (?)

% pinhole emittances before measurement
pinholes_list = sr.pinholename(1:5);
for iph =  1:length(pinholes_list)
    try
        phdev_list{iph} = tango.Device(pinholes_list{iph}); % get devices
    catch err
        warning(['impossible to get device pinhole: ' pinholes_list{iph}])
        disp(err)
    end
end


ey = NaN(size( pinholes_list));
ex = NaN(size( pinholes_list));

for ip = 1:length( pinholes_list)
    try
        ph =  phdev_list{ip};
        ex(ip) = ph.Emittance_H.read;
        ey(ip) = ph.Emittance_V.read;
    catch errpin
        disp(['impossible to read device pinhole: '  pinholes_list{ip}])
        %disp(errpin)
    end
end

save(fullfile(workingdirectory,['ApparentEmittances' measurementtime]),'pinholes_list','ex','ey');
iax_values=ey;
save(fullfile(workingdirectory,['iaxemittance']),'iax_values');


if doplot
    figure('name','emittances [pm]');
    bar([ex;ey]' *1e12);
    bp = gca;
    legend('hor','ver');
    bp.XTick=1:length( pinholes_list);
    bp.XTickLabels= pinholes_list;
    bp.XTickLabelRotation = 45;
end
end