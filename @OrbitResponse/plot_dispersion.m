function varargout=plot_dispersion(obj,varargin)
%PLOT_DISPERSION	Plot the measured of model dispersion

% Process arguments
if ~isempty(varargin) && isa(varargin{1},'matlab.graphics.axis.Axes')
    ax=varargin{1};
    varargs=varargin(2:end);
else
    ax=gca;
    varargs=varargin;
end
[ylab,varargs]=getoption(varargs,'YLabel','Dispersion [m]');
[ihv,varargs]=getargs(varargs,1);

% Get fresp values
lines=arrayfun(@(ob) dispersion(ob,ihv),obj,'UniformOutput',false);

% Plot
nbpms=size(lines{1},1);
[varargout{1:nargout}]=plot(ax,cat(2,lines{:}),varargs{:});
set(ax,'XLim',[1 nbpms]);
ylabel(ax,ylab);
xlabel(ax,'BPM #');
grid(ax,'on');

    function d=dispersion(obj,ihv)
        fresp=[real(obj.fresp) imag(obj.fresp)];
        d=-obj.atmodel_.alpha.*fresp(:,ihv);
    end
end
