function correctdrift = driftcontrol(obj)
% measure current orbit and compare to intial reference
% returns true if  orbit is drifting more than threshold

correctdrift = false;
disp('check orbit drift compared to reference measurement before RM'); pause(2);

ocheck = obj.measfun();  % reference orbit measurement.

[~, st, ~ ] = obj.checkorbit(ocheck,obj.o0);
if any(st > obj.threshold_orbit_drifting)
    warning(['Orbit is drifting. std(COD-REF) (h,v,sum)'...
        num2str(st') ' ,'...
        'Probably one steerer did not return to its nominal value or something else is moving.'])
    correctdrift = true;
end
end